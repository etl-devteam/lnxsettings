#include "eventsManager.h"
#include <QTextStream>
#include <QProcess>
#include <QFile>
#include <QDebug>


const int FREQUENCY_TIMER=50;

EventsManager::EventsManager(QObject *parent) :
    QObject(parent),
    location_sys_class(""),
    timer(new QTimer(this)),
    choice_input_key1(0),
    choice_input_key2(0),
    choice_input_key3(0),
    choice_input_key4(0),
    old_choice_input_key1(0),
    old_choice_input_key2(0),
    old_choice_input_key3(0),
    old_choice_input_key4(0),
    selectedOption(true),
    modifyMode(false)
{

#if !DEBUG
    previeniCavoStaccato();

    //Creo la directory dove deposito tutti i file di log e gli script dell'applicazione
    QProcess shell;

    if( QFile::exists(log_dir+"/WifiManager") ){
        shell.execute("/bin/sh", QStringList() << "-c" << "rm -r "+log_dir+"/DisplaySettings");
    }


    shell.execute("/bin/sh", QStringList() << "-c" << "mkdir -p "+log_dir+"/DisplaySettings/Scripts");
    shell.close();


#else


    QProcess shell;

    if( QFile::exists(log_dir) ){
        shell.execute("/bin/sh", QStringList() << "-c" << "rm -r "+log_dir+"/DisplaySettings");
    }

    shell.execute("/bin/sh", QStringList() << "-c" << "mkdir -p "+log_dir+"/DisplaySettings/sys/class/dinema");
    shell.execute("/bin/sh", QStringList() << "-c" << "mkdir -p "+log_dir+"/DisplaySettings/sys/class/usbmisc");
    shell.execute("/bin/sh", QStringList() << "-c" << "mkdir -p "+log_dir+"/DisplaySettings/sys/class/graphics/fb0");
    shell.execute("/bin/sh", QStringList() << "-c" << "echo 30 > "+log_dir+"/DisplaySettings/sys/class/graphics/fb0/contrast");
    shell.execute("/bin/sh", QStringList() << "-c" << "echo 40 > "+log_dir+"/DisplaySettings/sys/class/graphics/fb0/brightness");
    shell.execute("/bin/sh", QStringList() << "-c" << "touch "+log_dir+"/DisplaySettings/sys/class/dinema/led_RGB");
    shell.execute("/bin/sh", QStringList() << "-c" << "mkdir -p "+log_dir+"/DisplaySettings/Scripts");


    shell.close();

    location_sys_class = QString(log_dir+"/DisplaySettings");

#endif

    accendiLedBlue();

    /**************** Connecting timer to private slot tasks() ******************************/
    connect(timer, SIGNAL(timeout()), this, SLOT(task()));

}

EventsManager::~EventsManager(){
    qDebug() << "Distruttore EventsManager";
    delete timer;
}

void EventsManager::stopTimer(){
    qDebug() << "EventsManager: stopping timer...";
    timer->stop();
}

void EventsManager::avviaEventsMng(){

// Inizializzo il DisplayManager
#if DEBUG
    emit setContrastFile(log_dir+"/DisplaySettings/sys/class/graphics/fb0/contrast");
    emit setBrightnessFile(log_dir+"/DisplaySettings/sys/class/graphics/fb0/brightness");
#else
    emit setContrastFile("/sys/class/graphics/fb0/contrast");
    emit setBrightnessFile("/sys/class/graphics/fb0/brightness");
#endif

    emit initDisplayManager();

// Inizio ad ascoltare eventi
    timer->start(FREQUENCY_TIMER);

}


void EventsManager::identificaEventoTastoRicevuto(){
    if( choice_input_key1 != old_choice_input_key1 ){
        old_choice_input_key1 = choice_input_key1;

        if( choice_input_key1 == 1 ){
        //Azione da intraprendere alla pressione del tasto 1 della ControlUnit

            if(!modifyMode){

                selectedOption = !selectedOption;
                emit changeSelection(selectedOption);

            }
            else{
                if(selectedOption){
                    emit changeContrastValue(true);
                }
                else{
                    emit changeBrightnessValue(true);
                }
            }

#if DEBUG
            emit tastoLetto();
#endif
        }
    }
    else if( choice_input_key2 != old_choice_input_key2 ){
        old_choice_input_key2 = choice_input_key2;

        if( choice_input_key2 == 1 ){
        //Azione da intraprendere alla pressione del tasto 2 della ControlUnit
            if(!modifyMode){
                modifyMode = true;
                emit startModifyMode(selectedOption);
            }

#if DEBUG
            emit tastoLetto();
#endif

        }
    }
    else if( choice_input_key3 != old_choice_input_key3 ){
        old_choice_input_key3 = choice_input_key3;

        if( choice_input_key3 == 1 ){
        //Azione da intraprendere alla pressione del tasto 3 della ControlUnit
            if(!modifyMode){

                selectedOption = !selectedOption;
                emit changeSelection(selectedOption);

            }
            else{
                if(selectedOption){
                    emit changeContrastValue(false);
                }
                else{
                    emit changeBrightnessValue(false);
                }
            }

#if DEBUG
            emit tastoLetto();
#endif
        }
    }
    else if( choice_input_key4 != old_choice_input_key4 ){
        old_choice_input_key4 = choice_input_key4;

        if( choice_input_key4 == 1 ){
        //Azione da intraprendere alla pressione del tasto 4 della ControlUnit
            if( modifyMode ){
                modifyMode = false;
                emit stopModifyMode();
            }
            else{
                emit chiudiApplicazione();
            }

#if DEBUG
            emit tastoLetto();
#endif

        }
    }
}

void EventsManager::letturaTasti(){
    //here there is the association between button and variable. 0 if NOT-pressed, 1 if pressed

    QFile input_key1(location_sys_class+"/sys/class/dinema/input_key1");            // open file 1 -lower button
    input_key1.open(QIODevice::ReadOnly | QIODevice::Text);

    QFile input_key2(location_sys_class+"/sys/class/dinema/input_key2");            // open file 2
    input_key2.open(QIODevice::ReadOnly | QIODevice::Text);

    QFile input_key3(location_sys_class+"/sys/class/dinema/input_key3");            // open file 3
    input_key3.open(QIODevice::ReadOnly | QIODevice::Text);

    QFile input_key4(location_sys_class+"/sys/class/dinema/input_key4");            // open file 4   -higher button
    input_key4.open(QIODevice::ReadOnly | QIODevice::Text);

    QTextStream stream1(&input_key1);
    QString numberS1 = stream1.readAll();
    choice_input_key1=numberS1.toInt();

    QTextStream stream2(&input_key2);
    QString numberS2 = stream2.readAll();
    choice_input_key2=numberS2.toInt();

    QTextStream stream3(&input_key3);
    QString numberS3 = stream3.readAll();
    choice_input_key3=numberS3.toInt();

    QTextStream stream4(&input_key4);
    QString numberS4 = stream4.readAll();
    choice_input_key4=numberS4.toInt();

    input_key1.close();
    input_key2.close();
    input_key3.close();
    input_key4.close();

}

void EventsManager::riabilitaBtns(){
#if DEBUG
    emit tastoLetto();
#endif
}

void EventsManager::chiusuraForzata(){
    QFile led_battery_file(location_sys_class+"/sys/class/dinema/led_RGB");
    led_battery_file.open(QIODevice::WriteOnly| QIODevice::Text);
    led_battery_file.write("8");
    emit chiudiApplicazione();
}

void EventsManager::accendiLedBlue(){
    /*********************************************************************************************/
    // Turn on blue LED to indicate that the app is running. Turn off when application is closed

    QFile led_battery_file(location_sys_class+"/sys/class/dinema/led_RGB");
    led_battery_file.open(QIODevice::WriteOnly| QIODevice::Text);
    led_battery_file.write("4");
    led_battery_file.close();

}


void EventsManager::task(){
    if (!QFile::exists(location_sys_class+"/sys/class/usbmisc/")){                     //controllo cavo staccato
         chiusuraForzata();
     }
     else {

        letturaTasti();
        identificaEventoTastoRicevuto();

     }
}

