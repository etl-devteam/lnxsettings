#ifndef DISPLAYMANAGER_H
#define DISPLAYMANAGER_H

#include <QObject>

class DisplayManager : public QObject
{
    Q_OBJECT

    public:
        explicit DisplayManager(QObject *parent = nullptr);
        ~DisplayManager();

    private:
        int contrast_value;
        int brightness_value;
        QString contrast_file;
        QString brightness_file;

        /*********************************************************/

        void writeValueOnContrastFile();
        void writeValueOnBrightnessFile();

    signals:
        void changeSliderContrastValue(int);      //true alza, false abbassa
        void changeSliderBrightnessValue(int);    //true alza, false abbassa

    public slots:
        void initDisplayManager();
        void setContrastFile(QString);
        void setBrightnessFile(QString);
        void changeContrastValue(bool);      //true alza, false abbassa
        void changeBrightnessValue(bool);    //true alza, false abbassa


};

#endif // DISPLAYMANAGER_H
