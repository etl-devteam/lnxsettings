#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    public:
        explicit MainWindow(QString nome_dir_icone_app, QWidget *parent = nullptr);
        ~MainWindow();


        void avviaGui();

    private:
        Ui::MainWindow *ui;
        QPixmap logo;
        QPixmap contrast_active_img;
        QPixmap bright_active_img;

        /************************************************************************/

         void configuraSchermataPrincipale();

         void selectContrastOpt();
         void selectBrightnessOpt();


     signals:
         void operazioneCompletata(); // emesso al termine di un operazione richiesta da un btn premuto
         void avviaConfigurazioneIniziale();

    private slots:
        void configurazioneInizialeGui();

    public slots:
        void changeSelection(bool);
        void startModifyMode(bool);
        void stopModifyMode();
        void changeSliderContrastValue(int);
        void changeSliderBrightnessValue(int);


};

#endif // MAINWINDOW_H
