#include "displayManager.h"
#include <QFile>
#include <QDebug>

DisplayManager::DisplayManager(QObject *parent) :
    QObject(parent)
{

}

void DisplayManager::initDisplayManager(){
    /* Inizializzo il valore del contrast */
    QFile contrastFile(contrast_file);
    if( contrastFile.open(QIODevice::ReadOnly| QIODevice::Text) ){
        QTextStream stream_contrastFile( &contrastFile );

        QString line = stream_contrastFile.readLine();

        contrast_value = line.toInt();


    }
    contrastFile.close();

    /* Inizializzo il valore della brightness */
    QFile brightnessFile(brightness_file);
    if( brightnessFile.open(QIODevice::ReadOnly| QIODevice::Text) ){
        QTextStream stream_brightnessFile( &brightnessFile );

        QString line = stream_brightnessFile.readLine();

        brightness_value = line.toInt();

    }
    brightnessFile.close();

    changeSliderContrastValue(contrast_value);
    changeSliderBrightnessValue(brightness_value);
}

DisplayManager::~DisplayManager(){
    qDebug() << "Distruttore CUBtnsSimulator";

}

void DisplayManager::setContrastFile(QString file){
    contrast_file = file;
}

void DisplayManager::setBrightnessFile(QString file){
    brightness_file = file;
}

void DisplayManager::changeContrastValue(bool up){



    if(up){
        if( contrast_value+10 <= 250 ){
            contrast_value += 10;
            emit changeSliderContrastValue(contrast_value);
            writeValueOnContrastFile();
        }
    }
    else{
        if( contrast_value-10 > 10 ){
            contrast_value -= 10;
            emit changeSliderContrastValue(contrast_value);
            writeValueOnContrastFile();
        }
    }
}

void DisplayManager::changeBrightnessValue(bool up){
    if(up){
        if( brightness_value+10 < 250 ){
            brightness_value += 10;
            emit changeSliderBrightnessValue(brightness_value);
            writeValueOnBrightnessFile();
        }
    }
    else{
        if( brightness_value-10 >= 10 ){
            brightness_value -= 10;
            emit changeSliderBrightnessValue(brightness_value);
            writeValueOnBrightnessFile();
        }
    }
}

void DisplayManager::writeValueOnContrastFile(){
    QFile w_file(contrast_file);
    if(w_file.open(QIODevice::ReadWrite| QIODevice::Text)){
        QTextStream outputFileStream(&w_file);
        outputFileStream << contrast_value;
    }
    w_file.close();
}

void DisplayManager::writeValueOnBrightnessFile(){
    QFile w_file(brightness_file);
    if(w_file.open(QIODevice::ReadWrite| QIODevice::Text)){
        QTextStream outputFileStream(&w_file);
        outputFileStream << brightness_value;
    }
    w_file.close();
}

