#include "threadsManager.h"

ThreadsManager::ThreadsManager(QString /* nome_dir_binario */,
                               QString /* nome_dir_icone_app */,
                               MainWindow *gui,
                               QApplication *qAppl,
                               QObject *parent) :
    QObject(parent),
    gui(gui),
    qAppl(qAppl),
    threadEvMng(new QThread()),
    threadDisplayMng(new QThread()),
    evMng(new EventsManager()),
    displayMng(new DisplayManager())
{
    //Can't touch this (oh-oh oh oh oh-oh-oh)

    inizializzazioneCustom();


    /*************************************************************************************/

    evMng->moveToThread(threadEvMng);

    QObject::connect(threadEvMng, SIGNAL(started()), evMng, SLOT(avviaEventsMng()));
    QObject::connect(threadEvMng, SIGNAL(finished()), evMng, SLOT(deleteLater()));
    QObject::connect(gui, SIGNAL(operazioneCompletata()), evMng, SLOT(riabilitaBtns()));
    QObject::connect(evMng, SIGNAL(chiudiApplicazione()), this, SLOT(chiudiApplicazione()));
    QObject::connect(this, SIGNAL(stopTimer()), evMng, SLOT(stopTimer()));

    /*************************************************************************************/

#if DEBUG
    cubSim= new CUBtnsSimulator(CU_FILE_DEVICE, evMng->location_sys_class);
    threadCubSim = new QThread();

    cubSim->moveToThread(threadCubSim);
    QObject::connect(threadCubSim, SIGNAL(started()), cubSim, SLOT(avviaSimulator()));
    QObject::connect(threadCubSim, SIGNAL(finished()), cubSim, SLOT(deleteLater()));
    QObject::connect(evMng, SIGNAL(tastoLetto()), cubSim, SLOT(azzera_input_keys()));
    QObject::connect(this, SIGNAL(stopTimer()), cubSim, SLOT(stopTimer()));


#endif

#if USE_TOUCH
    touchMng = new TouchManager(TOUCH_FILE_DEVICE);
    threadTouchMng = new QThread();

    touchMng->moveToThread(threadTouchMng);

    QObject::connect( threadTouchMng, SIGNAL(started()), touchMng, SLOT(avviaTouchManager()));
    QObject::connect( threadTouchMng, SIGNAL(finished()), touchMng, SLOT(deleteLater()));
    QObject::connect( evMng, SIGNAL(abilitaTouchManager()), touchMng, SLOT(abilitaTouch()));
    QObject::connect( touchMng, SIGNAL(tastoPremuto(int)), evMng, SLOT(eventFromTouch(int)));
    QObject::connect(this, SIGNAL(stopTimer()), touchMng, SLOT(stopTimer()));


#endif

    startApplication();

}

void ThreadsManager::startApplication(){
    gui->avviaGui();

#if DEBUG
    threadCubSim->start();
#endif

#if USE_TOUCH
    threadTouchMng->start();
#endif

    threadEvMng->start();

    startCustomThreads();
}


void ThreadsManager::chiudiApplicazione(){
    //Can't touch this (oh-oh oh oh oh-oh-oh)

     emit stopTimer();

#if DEBUG

    threadCubSim->quit();
    threadCubSim->wait();

    delete  threadCubSim;
#endif

#if USE_TOUCH

    threadTouchMng->quit();
    threadTouchMng->wait();

    delete  threadTouchMng;
#endif

    threadEvMng->quit();
    threadEvMng->wait();

    delete threadEvMng;


    destroyCustomThreads();

    qAppl->exit();
}


void ThreadsManager::inizializzazioneCustom(){

    displayMng->moveToThread(threadDisplayMng);

    QObject::connect(threadDisplayMng, SIGNAL(finished()), displayMng, SLOT(deleteLater()));
    QObject::connect(displayMng, SIGNAL(changeSliderContrastValue(int)), gui, SLOT(changeSliderContrastValue(int)));
    QObject::connect(displayMng, SIGNAL(changeSliderBrightnessValue(int)), gui, SLOT(changeSliderBrightnessValue(int)));
     QObject::connect(evMng, SIGNAL(initDisplayManager()), displayMng, SLOT(initDisplayManager()));
    QObject::connect(evMng, SIGNAL(setContrastFile(QString)), displayMng, SLOT(setContrastFile(QString)));
    QObject::connect(evMng, SIGNAL(setBrightnessFile(QString)), displayMng, SLOT(setBrightnessFile(QString)));
    QObject::connect(evMng, SIGNAL(changeContrastValue(bool)), displayMng, SLOT(changeContrastValue(bool)));
    QObject::connect(evMng, SIGNAL(changeBrightnessValue(bool)), displayMng, SLOT(changeBrightnessValue(bool)));



    QObject::connect(evMng, SIGNAL(changeSelection(bool)), gui, SLOT(changeSelection(bool)));
    QObject::connect(evMng, SIGNAL(startModifyMode(bool)), gui, SLOT(startModifyMode(bool)));
    QObject::connect(evMng, SIGNAL(stopModifyMode()), gui, SLOT(stopModifyMode()));

}

void ThreadsManager::startCustomThreads(){

    threadDisplayMng->start();
}

void ThreadsManager::destroyCustomThreads(){

    threadDisplayMng->quit();
    threadDisplayMng->wait();

    delete threadDisplayMng;

    QProcess shell;
    shell.execute("/bin/sh", QStringList() << "-c" << log_dir+"/rilanciaSettingsManager.sh &");
    shell.close();

}
