#include "mainWindow.h"
#include "envheader.h" // INCLUSO SOLO PER IMPORTARE IL DEFINE DEBUG
#include "ui_mainWindow.h"
#include <QDebug>


#if DEBUG
    const QString coloreVisibile = "#7fff00";
#else
    const QString coloreVisibile = "white";
#endif


MainWindow::MainWindow(QString nome_dir_icone_app, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    logo(nome_dir_icone_app+"/VisionAr.png"),
    contrast_active_img(nome_dir_icone_app+"/arrow_settings.png"),
    bright_active_img(nome_dir_icone_app+"/arrow_settings.png")
{
    ui->setupUi(this);

    connect( this, SIGNAL(avviaConfigurazioneIniziale()), this, SLOT(configurazioneInizialeGui()));

}

MainWindow::~MainWindow()
{
    qDebug() << "Distruttore MainWindow";
    delete ui;
}

void MainWindow::avviaGui(){
    emit avviaConfigurazioneIniziale();
    this->show();
}

void MainWindow::configurazioneInizialeGui(){

    ui->background->hide();
    ui->background->setStyleSheet("QWidget { background-color : black; color : "+coloreVisibile+"; }");

    configuraSchermataPrincipale();

    ui->background->show();
}

void MainWindow::configuraSchermataPrincipale(){

    ui->icon->setPixmap(logo.scaled(ui->icon->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
    ui->contrast_confirm->setPixmap(contrast_active_img.scaled(ui->contrast_confirm->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
    ui->bright_confirm->setPixmap(bright_active_img.scaled(ui->bright_confirm->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));

    ui->contrast_label->setText("> CONTRAST <");
    ui->bright_label->setText("> BRIGHTNESS <");

    ui->contrast_slider->setStyleSheet("selection-background-color: "+coloreVisibile+"; border: 0;");
    ui->contrast_slider->setValue(0);
    ui->contrast_slider->clearFocus();

    ui->bright_slider->setStyleSheet("selection-background-color: "+coloreVisibile+";");
    ui->bright_slider->setValue(0);
    ui->bright_slider->clearFocus();

    ui->bright_confirm->hide();
    ui->contrast_confirm->hide();

    selectContrastOpt();
}

void MainWindow::selectContrastOpt(){

    ui->contrast_label->setStyleSheet("border: 1px solid "+coloreVisibile+";");
    ui->bright_label->setStyleSheet("border: 0px");

}

void MainWindow::selectBrightnessOpt(){

    ui->contrast_label->setStyleSheet("border: 0px");
    ui->bright_label->setStyleSheet("border: 1px solid "+coloreVisibile+";");

}


void MainWindow::changeSelection(bool who){

    if(who){
        selectContrastOpt();
    }
    else{
        selectBrightnessOpt();
    }

}

void MainWindow::startModifyMode(bool who){

    if(who){
        ui->contrast_confirm->show();
    }
    else{
         ui->bright_confirm->show();
    }
}

void MainWindow::stopModifyMode(){
    ui->contrast_confirm->hide();
    ui->bright_confirm->hide();
}


void MainWindow::changeSliderContrastValue(int val){
    ui->contrast_slider->setValue(val);
}

void MainWindow::changeSliderBrightnessValue(int val){
    ui->bright_slider->setValue(val);
}
