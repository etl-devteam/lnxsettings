#include "touchManager.h"
#include "eventsManager.h"  // INCLUSO SOLO PER IMPORTARE IL DEFINE DEBUG
#include <QDebug>

const int POLLING_FREQUENCY=10;  // frequency of refresh of State Machine

TouchManager::TouchManager(QString device, QObject *parent) :
    QObject(parent),
    device(device),
    fd(0),
    size(sizeof(struct input_event)),
    touch_abilitato(false),
    device_connection_closed(true),
    second_read(false),
    polling_timer(this)
{
    /**************** Connecting timer to private slot windowTasks() ******************************/

    connect(&polling_timer, SIGNAL(timeout()), this, SLOT(ascoltaTouch()));

    /***********************************************************************************************/

}

TouchManager::~TouchManager(){
    qDebug() << "Distruttore TouchManager";
    QT_CLOSE(fd);

}

void TouchManager::stopTimer(){
    polling_timer.stop();
}

void TouchManager::avviaTouchManager(){
    polling_timer.start(POLLING_FREQUENCY);

}

void TouchManager::abilitaTouch(){
    touch_abilitato = true;
    qDebug() << "TouchManager: lettura tasto riabilitata.";
}

void TouchManager::disabilitaTouch(){
    touch_abilitato = false;
    qDebug() << "TouchManager: lettura tasto disabilitata.";
}


void TouchManager::ascoltaTouch(){
// Ogni volta che avviene un evento viene prima inviato il codice 4,
// poi il codice del tasto premuto e infine il codice 0;

    if(touch_abilitato){
        if( device_connection_closed ){
            fd = QT_OPEN(device.toLocal8Bit().constData(), O_RDONLY | O_NDELAY, 0);

            if(fd >= 0)
            {
                 // Gain exclusive access to the input_event file
            //    ::ioctl(fd, EVIOCGRAB, 1);
                device_connection_closed = false;
            }

        }



        // Shouldn't happen, but you never know
        if (( QT_READ(fd, ev, size)) < size) {
            return;
        }

        int codiceTasto = 0;
        if( ev[0].code == 4 ){
            // Shouldn't happen, but you never know
            if (( QT_READ(fd, ev, size)) < size) {
                return;
            }

            codiceTasto = ev[0].code;

            if (( QT_READ(fd, ev, size)) < size) { //Leggo l'evento 0
                return;
            }
        }

        if( second_read ){
            second_read = false;
            return;
        }
        else{
            second_read = true;
        }

        qDebug() << "Event from CUBtnsSimulator: #"+QString::number(codiceTasto);


        if( codiceTasto != 2 && codiceTasto != 3 && codiceTasto != 4 && codiceTasto != 5 ) // se premo i tasti riservati al CUBtnsSimulator li ignoro
        {
            qDebug() << "Event from TouchManager: #"+QString::number(codiceTasto);

            touch_abilitato = false;
            QT_CLOSE(fd);
            device_connection_closed = true;
            emit tastoPremuto(codiceTasto);

        }


    }
}

