#ifndef NETWORKMANAGER_H
#define NETWORKMANAGER_H

#include <QObject>
#include <QProcess>
#include <QTimer>

class NetworkManager : public QObject
{
    Q_OBJECT

    public:
        explicit NetworkManager(QString file_reti_scansionate,
                                QString file_encryption_keys,
                                QString name_network_interface,
                                QObject *parent = nullptr);
        ~NetworkManager();

    private:
        QString file_reti_scansionate;    // dove vengono memorizzati i nomi di rete trovati
        QString file_encryption_keys;     // dove vengono memorizzati i tipi di criptazione delle reti
        bool connessioneFallita;          // true se e' stata bloccata per qualsiasi motivo la connessione
        bool connessioneVerificata;       //usata in fase di scanning per fare una sola volta all'inizio la verifica preconnessione
        QTimer timer_verifica_connessione;
        QString name_network_interface;
        int counterRescanning;

        /**************************************************************************/

        void generaScriptVerificaPreConnessione();
        void generaScriptCreaFileLog();

    signals:
        void listaSSIDAggiornata();
        void verificaSuccessoConnessione();
        void confermaSuccessoConnessione();

    public slots:
        void stopTimer();
        void aggiornaListaSSID();
        void connessione(QString, QString);

    private slots:
        void disabilitaConfermaConnessione();
        void controlloConnessioneStabilita();
        //void start_timer_errore_connessione();
       // void timeout_timer_errore();

};

#endif // NETWORKMANAGER_H
