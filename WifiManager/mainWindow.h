#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVBoxLayout>
#include <QPushButton>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    public:
        explicit MainWindow(QString nome_dir_icone_app, QWidget *parent = nullptr);
        ~MainWindow();


        void avviaGui();

    private:
        Ui::MainWindow *ui;

        QPixmap wifi_logo;
        int selectedBtn;            // per ricordare quale btn è stato selezionato;
        QWidget *scrollContent;     // scrollContent e layout sono necessari per poter aggiungere/togliere
        QVBoxLayout *layout;        // dinamicamente bottoni dalla scrollArea
        QList<QPushButton*> networksButtons; // dove vengono memorizzati i btn con i nomi delle reti;
        QList<QString> listaEncryptionKeys;
        QTimer *timer_post_connessione;
        QTimer *timer_alert_window;
        QTimer *timer_errore_connessione;
        bool already_connected;

        /************************************************************************/

         void configuraSchermataPrincipale();

         void configuraSchermataCredenziali();
         void configuraSchermataConnessione();
         void configuraAlertWindow();
         void mostraAlertWindow();
         void pulisciNetworksButtons();
         void verificaConnessione();


     signals:
         void operazioneCompletata(); // emesso al termine di un operazione richiesta da un btn premuto
         void avviaConfigurazioneIniziale();

         void primoAvvio();
         void networksButtonsAggiornata();
         void scrollAreaNascosta();
         void scrollAreaPronta();
         void inizioConnessione(QString ssid, QString pass);
         void abilita_possibilita_interruzione_connessione();

    public slots:
        void mostraSchermataPrincipale();
        void mostraSchermataCredenziali(bool);
        void mostraSchermataConnessione();
        void nascondiScrollArea();
        void mostraScrollArea();
        void aggiornaNetworksButtons();
        void spostaSelezioneRete(bool direzione);
        void connessioneAvvenuta();
        void interrompi_timer_alert_window();
        void mostra_possibilita_interruzione_connessione();

    private slots:
        void configurazioneInizialeGui();
        void credenzialiConfermate();
        void scadenza_timer_post_connessione();
        void scadenza_timer_alert_window();


};

#endif // MAINWINDOW_H
