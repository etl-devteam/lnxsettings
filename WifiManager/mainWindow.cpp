#include "mainWindow.h"
#include "names.h"
#include "ui_mainWindow.h"
#include <QTextStream>
#include <QScrollBar>
#include <QTimer>
#include <QProcess>
#include <QFile>
#include <QDebug>


#if DEBUG
    const QString coloreVisibile = "#7fff00";
#else
    const QString coloreVisibile = "white";
#endif

const int LATENZA_POST_CONNESSIONE=2500;
const int LATENZA_ALERT=8000;
const int LATENZA_ERRORE_CONNESSIONE=60000;

MainWindow::MainWindow(QString nome_dir_icone_app, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    wifi_logo(nome_dir_icone_app+"/wifi_logo.png"),
    selectedBtn(0),
    scrollContent(new  QWidget(this)),
    layout(new QVBoxLayout(scrollContent)),
    networksButtons(),
    timer_post_connessione(new QTimer(this)),
    timer_alert_window(new QTimer(this)),
    timer_errore_connessione(new QTimer(this)),
    already_connected(false)
{
    ui->setupUi(this);

    /*************** Configurazione scroll area ***************/
    layout->setSpacing(0);
    layout->setContentsMargins(0, 0, 0, 0);
    scrollContent->setLayout(layout);
    ui->scrollArea->setWidget(scrollContent);

    /**************** Connessione segnali/slot interne *********/
    connect( this, SIGNAL(avviaConfigurazioneIniziale()), this, SLOT(configurazioneInizialeGui()));
    connect( this, SIGNAL(networksButtonsAggiornata()), this, SLOT(mostraScrollArea()));
    connect( ui->pass_input, SIGNAL(returnPressed()), this, SLOT(credenzialiConfermate()));
    connect( this, SIGNAL(primoAvvio()), this, SLOT(mostraSchermataPrincipale()));
    connect( timer_post_connessione, SIGNAL(timeout()), this, SLOT(scadenza_timer_post_connessione()));
    connect( timer_alert_window, SIGNAL(timeout()), this, SLOT(scadenza_timer_alert_window()));
    connect( timer_errore_connessione, SIGNAL(timeout()), this, SLOT(mostra_possibilita_interruzione_connessione()));

}

MainWindow::~MainWindow()
{
    qDebug() << "Distruttore MainWindow";
    delete scrollContent;
    delete timer_post_connessione;
    delete timer_alert_window;
    delete timer_errore_connessione;
    delete ui;
}

void MainWindow::avviaGui(){
    emit avviaConfigurazioneIniziale();
    this->show();
}

void MainWindow::pulisciNetworksButtons(){
    for( QPushButton *btn : networksButtons ){ //libero la memoria occupata dalla creazione dinamica dei btns
        delete btn;
    }

    while( !networksButtons.isEmpty() ){ //tolgo ogni btn dalla lista di memorizzazione
        networksButtons.removeFirst();
    }

    while( !listaEncryptionKeys.isEmpty() ){ //tolgo ogni encryprion key dalla lista di memorizzazione
        listaEncryptionKeys.removeFirst();
    }

    delete layout;
}

void MainWindow::verificaConnessione(){
    QString connected_to_label;

    QProcess shell;
    shell.execute("/bin/sh",  QStringList() << "-c" << log_dir+"/WifiManager/Scripts/verificaPreConnessione.sh" );
    shell.close();
    QFile preconnectedLog(log_dir+"/WifiManager/preconnessione.log");
    if( preconnectedLog.open(QIODevice::ReadOnly| QIODevice::Text) ){

        QTextStream in( &preconnectedLog );
        QString line = in.readLine();

        if( line == "off/any  ") // lasciare il doppio spazio finale nella stringa
            connected_to_label = "Not connected.";
        else
            connected_to_label = "Connected to: \n"+line.mid(1, line.count()-4);


    }
    else{
        qDebug() << "Errore: apertura "+log_dir+"/WifiManager/preconnessione.log";
    }
    preconnectedLog.close();

    ui->connected_to_label->setText(connected_to_label);

}

void MainWindow::configurazioneInizialeGui(){

    ui->background->hide();
    ui->background->setStyleSheet("QWidget { background-color : black; color : "+coloreVisibile+"; }");

    configuraSchermataPrincipale();
    configuraSchermataCredenziali();
    configuraSchermataConnessione();
    configuraAlertWindow();

   /* QString connected_to_label;

    QProcess shell;
    shell.execute("/bin/sh",  QStringList() << "-c" << log_dir+"/WifiManager/Scripts/verificaPreConnessione.sh" );
    shell.close();
    QFile preconnectedLog(log_dir+"/WifiManager/preconnessione.log");
    if( preconnectedLog.open(QIODevice::ReadOnly| QIODevice::Text) ){

        QTextStream in( &preconnectedLog );
        QString line = in.readLine();

        if( line == "off/any  ") // lasciare il doppio spazio finale nella stringa
            connected_to_label = "Not connected.";
        else
            connected_to_label = "Connected to: \n"+line.mid(1, line.count()-4);


    }
    else{
        qDebug() << "Errore: apertura "+log_dir+"/WifiManager/preconnessione.log";
    }
    preconnectedLog.close();

    ui->connected_to_label->setText(connected_to_label);*/

    verificaConnessione();
    ui->background->show();
    emit primoAvvio();
}

void MainWindow::configuraSchermataPrincipale(){
    ui->wifi_logo->setPixmap(wifi_logo.scaled(ui->wifi_logo->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
    ui->scrollArea->setStyleSheet("background-color : black; color : "+coloreVisibile+";");
    ui->scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    ui->scrollArea->setWidgetResizable(true);
    ui->scanning_label->setStyleSheet("color: "+coloreVisibile+";");
    ui->scanning_label->setText("Scanning...");

}

void MainWindow::configuraSchermataCredenziali(){
    ui->pass_window->setStyleSheet("background-color : black; color : "+coloreVisibile+"; border: 1px solid "+coloreVisibile+";");
    ui->selected_ssid->setStyleSheet("color: "+coloreVisibile+"; border: 0;");
    ui->pass_input->setStyleSheet("color: white; border: 1px solid "+coloreVisibile+"");
    ui->pass_input->setPlaceholderText("password...");

}

void MainWindow::configuraSchermataConnessione(){
    ui->connecting_label->setText("Connecting...");
    ui->conn_window->setStyleSheet("background-color : black; color : "+coloreVisibile+"; border: 1px solid "+coloreVisibile+";");
    ui->connecting_label->setStyleSheet("color: "+coloreVisibile+"; border: 0;");
    ui->error_label->setStyleSheet("color: "+coloreVisibile+"; border: 0;");

}


void MainWindow::configuraAlertWindow(){
    ui->alert_window->setStyleSheet("background-color : black; color : "+coloreVisibile+"; border: 1px solid "+coloreVisibile+";");
    ui->no_pass_label->setStyleSheet("color: "+coloreVisibile+"; border: 0;");
    ui->no_pass_label->setText("You are connecting to\na not protected wifi.\nPress \"Back\" to stop or wait 8 sec.");
}

void MainWindow::mostraAlertWindow(){

    if( already_connected ){
      ui->no_pass_label->setText("You are already connected!\nPress \"Back\" to return to menu.");
      already_connected = false;
      operazioneCompletata();
    }
    else{
       configuraAlertWindow();
       timer_alert_window->start(LATENZA_ALERT);
    }

    ui->areaDx->hide();
    ui->areaSx->hide();
    ui->pass_window->hide();
    ui->conn_window->hide();
    ui->alert_window->show();

}

void MainWindow::mostraSchermataPrincipale(){
    ui->areaDx->show();
    ui->areaSx->show();
    ui->pass_window->hide();
    ui->conn_window->hide();
    ui->alert_window->hide();

}

void MainWindow::mostraSchermataCredenziali(bool pulizia_password){

    QStringList list = ui->connected_to_label->text().split(QRegExp("\\s+"), QString::SkipEmptyParts);

    QString nome_rete_attualmente_connessa=list.last();
//    nome_rete_attualmente_connessa.mid(1, nome_rete_attualmente_connessa.count()-1);

   if( networksButtons[selectedBtn]->text() == nome_rete_attualmente_connessa ){
        already_connected = true;
        mostraAlertWindow();
    }
   else{

        if( listaEncryptionKeys[selectedBtn] == "on" ){ // se la rete e' criptata
            if( pulizia_password ){
                ui->pass_input->setText("");
            }
            else{
                pulizia_password = true;
            }

            // Imposto il titolo nella finestra delle credenziali settandolo
            // al nome della rete associato al bottone
            ui->selected_ssid->setText(networksButtons[selectedBtn]->text());
            ui->areaDx->hide();
            ui->areaSx->hide();
            ui->conn_window->hide();
            ui->alert_window->hide();
            ui->pass_window->show();
            ui->pass_input->setFocus();

        }
        else{ // se la rete non e' criptata
            mostraAlertWindow();
            //mostraSchermataConnessione();
        }
   }
}

void MainWindow::mostraSchermataConnessione(){
    timer_errore_connessione->start(LATENZA_ERRORE_CONNESSIONE);

    ui->connecting_label->setText("Connecting...");
    ui->error_label->setText("Wrong password or network problem.\nPress \"Back\" to check your password.");
    ui->error_label->hide();
    ui->areaDx->hide();
    ui->areaSx->hide();
    ui->pass_window->hide();
    ui->alert_window->hide();
    ui->conn_window->show();

    if( listaEncryptionKeys[selectedBtn] == "on" )
        emit inizioConnessione(networksButtons[selectedBtn]->text(), ui->pass_input->text());
    else
        emit inizioConnessione(networksButtons[selectedBtn]->text(), "off");
}

void MainWindow::nascondiScrollArea(){
    ui->scrollArea->hide();
    ui->scanning_label->show();
    ui->scrollArea->verticalScrollBar()->setValue(0);
    selectedBtn = 0;

    emit scrollAreaNascosta();
}


void MainWindow::mostraScrollArea(){
    ui->scrollArea->show();
    ui->scanning_label->hide();

    emit scrollAreaPronta();
}

void MainWindow::aggiornaNetworksButtons(){
    pulisciNetworksButtons();


    QStringList listaSSID;

    QFile ssid_rilevati(file_reti_trovate);
    if( ssid_rilevati.open(QIODevice::ReadOnly| QIODevice::Text) ){

        QTextStream in( &ssid_rilevati );
        while(!in.atEnd()){
            QString line = in.readLine();
            line = line.mid(1, line.count()-2);
            listaSSID.push_back(line);
        }
    }
    ssid_rilevati.close();

    layout = new QVBoxLayout(scrollContent);
    scrollContent->setLayout(layout);
    ui->scrollArea->setWidget(scrollContent);

    bool firstButton = true;

    for( QString nome_rete : listaSSID ){
        QPushButton *new_btn = new QPushButton(nome_rete);

        if( firstButton ){ // seleziono il primo btn della lista
            firstButton = false;
            new_btn->setStyleSheet("background-color : "+coloreVisibile+"; color : black;");

        }

        networksButtons.push_back(new_btn);
        layout->addWidget(new_btn, 0, Qt::AlignTop);

    }

    //rinnovo la lista delle encryption keys
    QFile keys_rilevate(file_encryption_keys);
    if( keys_rilevate.open(QIODevice::ReadOnly| QIODevice::Text) ){

        QTextStream in( &keys_rilevate );
        while(!in.atEnd()){
            listaEncryptionKeys.push_back(in.readLine());
        }
    }
    keys_rilevate.close();

    verificaConnessione();

    emit networksButtonsAggiornata();
}


void MainWindow::spostaSelezioneRete(bool direzione){

    //modifico l'id del bottone nel campo selectedBtn;
    if( direzione ){ //sposto la selezione in basso
        if( selectedBtn < networksButtons.count()-1 ){
            selectedBtn++;
        }

        //sposto la scrollbar
        if( selectedBtn > 1 ){
            ui->scrollArea->verticalScrollBar()->setValue(ui->scrollArea->verticalScrollBar()->value()+30);
        }

    }
    else{ // sposto la selezione in alto
        if( selectedBtn > 0 ){
            selectedBtn--;
        }

        //sposto la scrollbar
        if( selectedBtn < networksButtons.count()-2){
            ui->scrollArea->verticalScrollBar()->setValue(ui->scrollArea->verticalScrollBar()->value()-30);
        }

    }

    // cambio l'estetica ai bottoni in base alla selezione fatta dall'utente
    int counter = 0;

    for( QPushButton *btn : networksButtons ){
        if( counter == selectedBtn ){
            btn->setStyleSheet("background-color : "+coloreVisibile+"; color : black;");

        }
        else{
            btn->setStyleSheet("background-color : black; color : "+coloreVisibile+";");

        }

        counter++;
    }

#if DEBUG
    emit operazioneCompletata();
#endif
}

void MainWindow::credenzialiConfermate(){
    mostraSchermataConnessione();
}

void MainWindow::connessioneAvvenuta(){
    ui->connecting_label->setText("Connected!");
    timer_errore_connessione->stop();
    timer_post_connessione->start(LATENZA_POST_CONNESSIONE);
}

void MainWindow::scadenza_timer_post_connessione(){
    timer_post_connessione->stop();

    QString connected_to_label;

    QProcess shell;
    shell.execute("/bin/sh",  QStringList() << "-c" << log_dir+"/WifiManager/Scripts/verificaPreConnessione.sh" );
    shell.close();
    QFile preconnectedLog(log_dir+"/WifiManager/preconnessione.log");
    if( preconnectedLog.open(QIODevice::ReadOnly| QIODevice::Text) ){

        QTextStream in( &preconnectedLog );
        QString line = in.readLine();

        if( line == "off/any  ") // lasciare il doppio spazio finale nella stringa
            connected_to_label = "Not connected.";
        else
            connected_to_label = "Connected to: \n"+line.mid(1, line.count()-4);


    }
    preconnectedLog.close();

    ui->connected_to_label->setText(connected_to_label );
    mostraSchermataPrincipale();

#if DEBUG
    emit operazioneCompletata();
#endif

}

void MainWindow::scadenza_timer_alert_window(){
    timer_alert_window->stop();
    mostraSchermataConnessione();

}

void MainWindow::interrompi_timer_alert_window(){
    timer_alert_window->stop();

}


void MainWindow::mostra_possibilita_interruzione_connessione(){
    ui->error_label->show();
    timer_errore_connessione->stop();
    emit abilita_possibilita_interruzione_connessione();
}


