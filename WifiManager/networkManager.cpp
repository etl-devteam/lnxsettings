#include "networkManager.h"
#include "envheader.h"
#include <QFile>
#include <QTextStream>
#include <QDebug>

const int LATENZA_VERIFICA_CONNESSIONE=50;


NetworkManager::NetworkManager(QString file_reti_scansionate,
                               QString file_encryption_keys,
                               QString name_network_interface,
                               QObject *parent) :
    QObject(parent),
    file_reti_scansionate(file_reti_scansionate),
    file_encryption_keys(file_encryption_keys),
    connessioneFallita(false),
    connessioneVerificata(false),
    timer_verifica_connessione(this),
    name_network_interface(name_network_interface),
    counterRescanning(0)
{
    QProcess shell;

    if( !QFile::exists(log_dir+"/WifiManager") ){  //Mi assicuro che esista la cartella in cui inserire i file di log e gli script
       shell.execute("/bin/sh", QStringList() << "-c" << "mkdir -p "+log_dir+"/WifiManager/Scripts");
       shell.execute("/bin/sh", QStringList() << "-c" << "mkdir -p "+log_dir+"/WifiManager/Wpa_supplicant/Interface");
    }

    shell.close();

    generaScriptVerificaPreConnessione();
    generaScriptCreaFileLog();

    /*******************************************************************************/

    connect(&timer_verifica_connessione, SIGNAL(timeout()), this, SLOT(controlloConnessioneStabilita()));
}

NetworkManager::~NetworkManager(){
    qDebug() << "Distruttore NetworkManager";
}

void NetworkManager::stopTimer(){
    qDebug() << "NetworkManager: stopping timer...";
    timer_verifica_connessione.stop();
}

void NetworkManager::generaScriptVerificaPreConnessione(){

    QFile w_file(log_dir+"/WifiManager/Scripts/verificaPreConnessione.sh");
    if(w_file.open(QIODevice::ReadWrite| QIODevice::Text)){
        QTextStream outputFileStream(&w_file);
        outputFileStream << "#!/bin/sh" << endl;
        outputFileStream << endl;

        outputFileStream << "ifconfig " + name_network_interface + " | grep inet | awk -F \" \" '{print $2}' > "+log_dir+"/WifiManager/ip.log" << endl;
        outputFileStream << "iwconfig " + name_network_interface + " | grep \"Point:\" | awk -F: '{ print $3 }' | awk '{ print $1 }' > " + log_dir + "/WifiManager/associated.log" << endl;
        outputFileStream << endl;

        outputFileStream << "IP=\"\"" << endl;
        outputFileStream << "NET_ASSOCIATED=\"\"" << endl;
        outputFileStream << "while read line; do" << endl;
        outputFileStream << "   IP=$line" << endl;
        outputFileStream << "done < " + log_dir + "/WifiManager/ip.log" << endl;
        outputFileStream << endl;

        outputFileStream << "while read line; do" << endl;
        outputFileStream << "   NET_ASSOCIATED=$line" << endl;
        outputFileStream << "done < " + log_dir + "/WifiManager/associated.log" << endl;
        outputFileStream << endl;

        outputFileStream << "if [ \"$IP\" == \"\" ]; then" << endl;
        outputFileStream << "    echo \"off/any  \" > " + log_dir + "/WifiManager/preconnessione.log" << endl;
        outputFileStream << "elif [ \"$NET_ASSOCIATED\" == \"Not-Associated\" ]; then" << endl;
        outputFileStream << "    echo \"off/any  \" > " + log_dir + "/WifiManager/preconnessione.log" << endl;
        outputFileStream << "else" << endl;
        outputFileStream << "    iwconfig " + name_network_interface + " | grep ESSID | awk -F: '{ print $2 }' > " + log_dir + "/WifiManager/preconnessione.log" << endl;
        outputFileStream << "fi" << endl;
    }
    w_file.close();

    QProcess shell;
    shell.execute("/bin/sh", QStringList() << "-c" << "chmod +x " + log_dir + "/WifiManager/Scripts/verificaPreConnessione.sh");
    shell.close();
}



void NetworkManager::generaScriptCreaFileLog(){
    QFile w_file(log_dir+"/WifiManager/Scripts/creaFileLog.sh");
    if(w_file.open(QIODevice::ReadWrite| QIODevice::Text)){
        QTextStream outputFileStream(&w_file);
        outputFileStream << "#!/bin/sh" << endl;
        outputFileStream << endl;

        outputFileStream << "rm " + log_dir + "/WifiManager/tmp.log" << endl;
        outputFileStream << "ifconfig " + name_network_interface + " down && ifconfig " + name_network_interface + " up" << endl;
        outputFileStream << "iwlist " + name_network_interface + " scan | grep -e \"ESSID\" -e 'Encryption key' | awk -F: '{ print $2 }' > "+log_dir+"/WifiManager/tmp.log" << endl;
    }
    w_file.close();

    QProcess shell;
    shell.execute("/bin/sh", QStringList() << "-c" << "chmod +x "+log_dir+"/WifiManager/Scripts/creaFileLog.sh");
    shell.close();
}

void NetworkManager::aggiornaListaSSID(){


    QProcess shell;

    if( !connessioneVerificata ){
        connessioneVerificata = true;
        shell.execute("/bin/sh",  QStringList() << "-c" << log_dir+"/WifiManager/Scripts/verificaPreConnessione.sh" );

    }


    if( counterRescanning == 4 || counterRescanning == 0){
        shell.execute("/bin/sh",  QStringList() << "-c" << "rmmod /wl18xx");
        shell.execute("/bin/sh",  QStringList() << "-c" << "insmod /lib/modules/$(uname -r)/kernel/drivers/net/wireless/ti/wl18xx/wl18xx.ko");

    }



    shell.execute("/bin/sh",  QStringList() << "-c" << log_dir+"/WifiManager/Scripts/creaFileLog.sh" );


    shell.close();



    QStringList listaSSID;
    QStringList listaEncryption;
    QString tmpEncryption;
    bool encryptionLine = true;

    QFile ssid_rilevati(log_dir+"/WifiManager/tmp.log");
    if( ssid_rilevati.open(QIODevice::ReadOnly| QIODevice::Text) ){

        QTextStream in( &ssid_rilevati );
        while(!in.atEnd()){
            if( encryptionLine ){
                encryptionLine = false;
                tmpEncryption = in.readLine();
            }
            else{
                encryptionLine = true;
                QString line = in.readLine();

                if( ! listaSSID.contains(line) && line != " " && line != "" && line != "\"\""){
                    listaEncryption.push_back(tmpEncryption);
                    listaSSID.push_back(line);

                }

            }
        }
    }
    ssid_rilevati.close();

    QFile ssid_file(file_reti_scansionate);
    if (ssid_file.open(QIODevice::ReadWrite)) {
        QTextStream stream(&ssid_file);

        for( QString rete : listaSSID ){
            stream << rete << endl;
        }

    }
    ssid_file.close();

    QFile keys_file(file_encryption_keys);
    if (keys_file.open(QIODevice::ReadWrite)) {
        QTextStream stream(&keys_file);

        for( QString key : listaEncryption ){
            stream << key << endl;
        }

    }
    keys_file.close();


    if( listaSSID.isEmpty() ){
        counterRescanning++;
        aggiornaListaSSID();
    }
    else{
        counterRescanning=0;
        connessioneVerificata = false;

        QProcess shell;
        shell.execute("/bin/sh",  QStringList() << "-c" << "killall udhcpc");


        QFile preconnectedLog(log_dir+"/WifiManager/preconnessione.log");
        if( preconnectedLog.open(QIODevice::ReadOnly| QIODevice::Text) ){

            QTextStream in( &preconnectedLog );
            QString line = in.readLine();

            if( line != "off/any  "){ // lasciare il doppio spazio finale nella stringa

                qDebug() << "Fine scanning: mi riconnetto alla rete";

#if DEBUG
                shell.execute("/bin/sh",  QStringList() << "-c" << "udhcpc -i "+name_network_interface+" " );

#else
                qDebug() << "Riconnessione post scanning";
                shell.execute("/bin/sh",  QStringList() << "-c" << "ifconfig "+name_network_interface+" 0.0.0.0 && rm "+log_dir+"/WifiManager/Wpa_supplicant/Interface/"+name_network_interface);
                shell.execute("/bin/sh",  QStringList() << "-c" << "wpa_supplicant -B -D wext -i "+name_network_interface+" -c  "+log_dir+"/WifiManager/Wpa_supplicant/wpa.conf");
                shell.execute("/bin/sh",  QStringList() << "-c" << "udhcpc -i "+name_network_interface+" -s /etc/udhcpc.script " );

#endif
            }
            else{
                qDebug() << "Fine scanning: non sei connesso a nulla " << line;
            }

        }


        shell.close();
        emit listaSSIDAggiornata();
    }
}

void NetworkManager::connessione(QString essid, QString pass){
    QProcess shell;

    //elimino il vecchio file di configurazione di wpa_supplicant
    shell.execute("/bin/sh",  QStringList() << "-c" << "rm "+log_dir+"/WifiManager/Wpa_supplicant/wpa.conf");

    //genero un nuovo file di configurazione per wpa_supplicant
    QFile confFile_wpa_supplicant(log_dir+"/WifiManager/Wpa_supplicant/wpa.conf");
    if (confFile_wpa_supplicant.open(QIODevice::ReadWrite)) {
        QTextStream stream(&confFile_wpa_supplicant);

        stream << "ctrl_interface="+log_dir+"/WifiManager/Wpa_supplicant/Interface" << endl;
        stream << "ctrl_interface_group=0" << endl;
        stream << endl;

        if( pass == "off"){
            stream << "network={" << endl;
            stream << "    ssid=\""+essid+"\"" << endl;
            stream << "  	scan_ssid=1" << endl;
            stream << "  	key_mgmt=NONE" << endl;
            stream << "}" << endl;
        }
        else{
            stream << "network={" << endl;
            stream << "    ssid=\""+essid+"\"" << endl;
            stream << "  	scan_ssid=1" << endl;
            stream << "  	key_mgmt=WPA-EAP WPA-PSK IEEE8021X NONE" << endl;
            stream << "  	psk=\""+pass+"\"" << endl;
            stream << "}" << endl;
        }


    }
    confFile_wpa_supplicant.close();


    // killo eventuali processi vecchi in esecuzione e disassocio da tutto l'interfaccia di rete
    shell.execute("/bin/sh",  QStringList() << "-c" << "killall udhcpc");
    shell.execute("/bin/sh",  QStringList() << "-c" << "killall wpa_supplicant");
    shell.execute("/bin/sh",  QStringList() << "-c" << "ifconfig "+name_network_interface+" down && ifconfig "+name_network_interface+" up");
    shell.execute("/bin/sh",  QStringList() << "-c" << "ifconfig "+name_network_interface+" 0.0.0.0 && rm "+log_dir+"/WifiManager/Wpa_supplicant/Interface/"+name_network_interface);

    // lancio i processi di sistema per la connessione
    shell.execute("/bin/sh",  QStringList() << "-c" << "wpa_supplicant -B -D wext -i "+name_network_interface+" -c  "+log_dir+"/WifiManager/Wpa_supplicant/wpa.conf");
    shell.execute("/bin/sh",  QStringList() << "-c" << "udhcpc -i "+name_network_interface+" -s /etc/udhcpc.script" );
    shell.close();

    timer_verifica_connessione.start(LATENZA_VERIFICA_CONNESSIONE);

}

void NetworkManager::disabilitaConfermaConnessione(){
    connessioneFallita = true;
}

void NetworkManager::controlloConnessioneStabilita(){
    timer_verifica_connessione.stop();
    if( !connessioneFallita ){

        QProcess shell;
        shell.execute("/bin/sh",  QStringList() << "-c" << log_dir+"/WifiManager/Scripts/verificaPreConnessione.sh" );
        shell.close();

        QFile preconnectedLog(log_dir+"/WifiManager/preconnessione.log");
        if( preconnectedLog.open(QIODevice::ReadOnly| QIODevice::Text) ){

            QTextStream in( &preconnectedLog );
            QString line = in.readLine();

            if( line != "off/any  "){ // lasciare il doppio spazio finale nella stringa

                emit confermaSuccessoConnessione();
            }
            else {
                qDebug() << "Connessione fallita! " << line;
            }
        }
    }
    else {
        connessioneFallita = false;

    }
}
