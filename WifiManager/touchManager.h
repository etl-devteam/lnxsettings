#ifndef TOUCHMANAGER_H
#define TOUCHMANAGER_H

#include <QObject>
#include <QTimer>
/***************************/
#include <sys/types.h>
#include <linux/hdreg.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <qplatformdefs.h>
#include "linux/input.h"

class TouchManager : public QObject
{
    Q_OBJECT

    public:
        explicit TouchManager(QString device, QObject *parent = nullptr);
        ~TouchManager();

    private:
        QString device;
        int fd;
        struct input_event ev[1]; //dove vengono memorizzati i codici dei tasti premuti
        int size;
        bool touch_abilitato;
        bool device_connection_closed;
        bool second_read; //non so come mai ma ogni bottone premuto invia due volte lo stesso evento tale flag evita che il secondo evento venga preso
        QTimer polling_timer;

    signals:
        void tastoPremuto(int codiceTasto);
        void incominciaAscolto();

    public slots:
        void avviaTouchManager();
        void abilitaTouch();
        void disabilitaTouch();
        void ascoltaTouch();
        void stopTimer();


};

#endif // TOUCHMANAGER_H
