#include "threadsManager.h"
#include "names.h"

ThreadsManager::ThreadsManager(QString /* nome_dir_binario */,
                               QString /* nome_dir_icone_app */,
                               MainWindow *gui,
                               QApplication *qAppl,
                               QObject *parent) :
    QObject(parent),
    gui(gui),
    qAppl(qAppl),
    threadEvMng(new QThread()),
    threadNetworkMng(new QThread()),
    evMng(new EventsManager()),
    networkMng(new NetworkManager(file_reti_trovate, file_encryption_keys, "wlan0")) // sulla cu impostare il terzo parametro a "wlan0"
{
    //Can't touch this (oh-oh oh oh oh-oh-oh)

    inizializzazioneCustom();


    /*************************************************************************************/

    evMng->moveToThread(threadEvMng);

    QObject::connect(threadEvMng, SIGNAL(started()), evMng, SLOT(avviaEventsMng()));
    QObject::connect(threadEvMng, SIGNAL(finished()), evMng, SLOT(deleteLater()));
    QObject::connect(gui, SIGNAL(operazioneCompletata()), evMng, SLOT(riabilitaBtns()));
    QObject::connect(evMng, SIGNAL(chiudiApplicazione()), this, SLOT(chiudiApplicazione()));
    QObject::connect(this, SIGNAL(stopTimer()), evMng, SLOT(stopTimer()));

    /*************************************************************************************/

#if DEBUG
    cubSim= new CUBtnsSimulator(CU_FILE_DEVICE, evMng->location_sys_class);
    threadCubSim = new QThread();

    cubSim->moveToThread(threadCubSim);
    QObject::connect(threadCubSim, SIGNAL(started()), cubSim, SLOT(avviaSimulator()));
    QObject::connect(threadCubSim, SIGNAL(finished()), cubSim, SLOT(deleteLater()));
    QObject::connect(evMng, SIGNAL(tastoLetto()), cubSim, SLOT(azzera_input_keys()));
    QObject::connect(this, SIGNAL(stopTimer()), cubSim, SLOT(stopTimer()));


#endif

#if USE_TOUCH
    touchMng = new TouchManager(TOUCH_FILE_DEVICE);
    threadTouchMng = new QThread();

    touchMng->moveToThread(threadTouchMng);

    QObject::connect( threadTouchMng, SIGNAL(started()), touchMng, SLOT(avviaTouchManager()));
    QObject::connect( threadTouchMng, SIGNAL(finished()), touchMng, SLOT(deleteLater()));
    QObject::connect( evMng, SIGNAL(abilitaTouchManager()), touchMng, SLOT(abilitaTouch()));
    QObject::connect( evMng, SIGNAL(disabilitaTouchManager()), touchMng, SLOT(disabilitaTouch()));
    QObject::connect( touchMng, SIGNAL(tastoPremuto(int)), evMng, SLOT(eventFromTouch(int)));
    QObject::connect(this, SIGNAL(stopTimer()), touchMng, SLOT(stopTimer()));


#endif

    startApplication();

}

void ThreadsManager::startApplication(){
    gui->avviaGui();

#if DEBUG
    threadCubSim->start();
#endif

#if USE_TOUCH
    threadTouchMng->start();
#endif

    threadEvMng->start();

    startCustomThreads();
}


void ThreadsManager::chiudiApplicazione(){
    //Can't touch this (oh-oh oh oh oh-oh-oh)

     emit stopTimer();

#if DEBUG

    threadCubSim->quit();
    threadCubSim->wait();

    delete  threadCubSim;
#endif

#if USE_TOUCH

    threadTouchMng->quit();
    threadTouchMng->wait();

    delete  threadTouchMng;
#endif

    threadEvMng->quit();
    threadEvMng->wait();

    delete threadEvMng;


    destroyCustomThreads();

    qAppl->exit();
}


void ThreadsManager::inizializzazioneCustom(){

    networkMng->moveToThread(threadNetworkMng);

    QObject::connect(threadNetworkMng, SIGNAL(finished()), networkMng, SLOT(deleteLater()));
    QObject::connect(evMng, SIGNAL(aggiornaListaSSID()), networkMng, SLOT(aggiornaListaSSID()));
    QObject::connect(evMng, SIGNAL(connettiAllaReteScelta(QString, QString)), networkMng, SLOT(connessione(QString, QString)));
    QObject::connect(networkMng, SIGNAL(listaSSIDAggiornata()), evMng, SLOT(aggiornamentoGuiPostScanning()));
    QObject::connect(networkMng, SIGNAL(confermaSuccessoConnessione()), evMng, SLOT(connessioneAvvenuta()));
    QObject::connect(evMng, SIGNAL(connessioneFallita()), networkMng, SLOT(disabilitaConfermaConnessione()) );

    QObject::connect(threadEvMng, SIGNAL(started()), evMng, SLOT(avviaEventsMng()));
    QObject::connect(threadEvMng, SIGNAL(finished()), evMng, SLOT(deleteLater()));
    QObject::connect( evMng, SIGNAL(inizioScanningRete()), gui, SLOT(nascondiScrollArea()));
    QObject::connect( evMng, SIGNAL(fineScanningRete()), gui, SLOT(aggiornaNetworksButtons()));
    QObject::connect( gui, SIGNAL(scrollAreaNascosta()), evMng, SLOT(scrollAreaNascosta()));
    QObject::connect( gui, SIGNAL(scrollAreaPronta()), evMng, SLOT(mainWindowPronta()));
    QObject::connect( gui, SIGNAL(inizioConnessione(QString, QString)), evMng, SLOT(avviaConnessione(QString, QString)));
    QObject::connect( evMng, SIGNAL(mostraSchermataCredenziali(bool)), gui, SLOT(mostraSchermataCredenziali(bool)));
    QObject::connect( evMng, SIGNAL(cambioReteSelezionata(bool)), gui, SLOT(spostaSelezioneRete(bool)));
    QObject::connect( evMng, SIGNAL(mostraSuccessoConnessione()), gui, SLOT(connessioneAvvenuta()));
    QObject::connect( evMng, SIGNAL(interrompiInserimentoCredenziali()), gui, SLOT(mostraSchermataPrincipale()));
    QObject::connect( evMng, SIGNAL(interrompi_timer_alert_window()), gui, SLOT(interrompi_timer_alert_window()));
    QObject::connect( gui, SIGNAL(abilita_possibilita_interruzione_connessione()), evMng, SLOT(pass_errata_rete_intasata()));
    QObject::connect(this, SIGNAL(stopTimer()), networkMng, SLOT(stopTimer()));
}

void ThreadsManager::startCustomThreads(){

    threadNetworkMng->start();

}

void ThreadsManager::destroyCustomThreads(){


    threadNetworkMng->quit();
    threadNetworkMng->wait();

    delete threadNetworkMng;

    QProcess shell;
    shell.execute("/bin/sh", QStringList() << "-c" << log_dir+"/rilanciaSettingsManager.sh &");
    shell.close();

}
