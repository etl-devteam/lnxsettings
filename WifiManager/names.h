#ifndef NAMES_H
#define NAMES_H

#include "envheader.h"

const QString file_reti_trovate(log_dir + "/WifiManager/reti_trovate.log");
const QString file_encryption_keys(log_dir + "/WifiManager/keys_trovate.log");

#endif // NAMES_H
