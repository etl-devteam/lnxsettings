#include "threadsManager.h"

ThreadsManager::ThreadsManager(QString /* nome_dir_binario */,
                               QString nome_dir_icone_app,
                               MainWindow *gui,
                               QApplication *qAppl,
                               QObject *parent) :
    QObject(parent),
    gui(gui),
    qAppl(qAppl),
    threadEvMng(new QThread()),
    evMng(new EventsManager()),
    vncMng(new VncManager(nome_dir_icone_app+"/../Confs"))
{
    //Can't touch this (oh-oh oh oh oh-oh-oh)

    inizializzazioneCustom();


    /*************************************************************************************/

    evMng->moveToThread(threadEvMng);

    QObject::connect(threadEvMng, SIGNAL(started()), evMng, SLOT(avviaEventsMng()));
    QObject::connect(threadEvMng, SIGNAL(finished()), evMng, SLOT(deleteLater()));
    QObject::connect(gui, SIGNAL(operazioneCompletata()), evMng, SLOT(riabilitaBtns()));
    QObject::connect(evMng, SIGNAL(chiudiApplicazione()), this, SLOT(chiudiApplicazione()));
    QObject::connect(this, SIGNAL(stopTimer()), evMng, SLOT(stopTimer()));

    /*************************************************************************************/

#if DEBUG
    cubSim= new CUBtnsSimulator(CU_FILE_DEVICE, evMng->location_sys_class);
    threadCubSim = new QThread();

    cubSim->moveToThread(threadCubSim);
    QObject::connect(threadCubSim, SIGNAL(started()), cubSim, SLOT(avviaSimulator()));
    QObject::connect(threadCubSim, SIGNAL(finished()), cubSim, SLOT(deleteLater()));
    QObject::connect(evMng, SIGNAL(tastoLetto()), cubSim, SLOT(azzera_input_keys()));
    QObject::connect(this, SIGNAL(stopTimer()), cubSim, SLOT(stopTimer()));


#endif

#if USE_TOUCH
    touchMng = new TouchManager(TOUCH_FILE_DEVICE);
    threadTouchMng = new QThread();

    touchMng->moveToThread(threadTouchMng);

    QObject::connect( threadTouchMng, SIGNAL(started()), touchMng, SLOT(avviaTouchManager()));
    QObject::connect( threadTouchMng, SIGNAL(finished()), touchMng, SLOT(deleteLater()));
    QObject::connect( evMng, SIGNAL(abilitaTouchManager()), touchMng, SLOT(abilitaTouch()));
    QObject::connect( touchMng, SIGNAL(tastoPremuto(int)), evMng, SLOT(eventFromTouch(int)));
    QObject::connect(this, SIGNAL(stopTimer()), touchMng, SLOT(stopTimer()));


#endif

    startApplication();

}

void ThreadsManager::startApplication(){
    gui->avviaGui();

#if DEBUG
    threadCubSim->start();
#endif

#if USE_TOUCH
    threadTouchMng->start();
#endif

    threadEvMng->start();

    startCustomThreads();
}


void ThreadsManager::chiudiApplicazione(){
    //Can't touch this (oh-oh oh oh oh-oh-oh)

     emit stopTimer();

#if DEBUG

    threadCubSim->quit();
    threadCubSim->wait();

    delete  threadCubSim;
#endif

#if USE_TOUCH

    threadTouchMng->quit();
    threadTouchMng->wait();

    delete  threadTouchMng;
#endif

    threadEvMng->quit();
    threadEvMng->wait();

    delete threadEvMng;


    destroyCustomThreads();

    qAppl->exit();
}


void ThreadsManager::inizializzazioneCustom(){

    vncMng->moveToThread(threadEvMng);

    QObject::connect( threadEvMng, SIGNAL(finished()), vncMng, SLOT(deleteLater()));
    QObject::connect(evMng, SIGNAL(configure_vncMng()), vncMng, SLOT(configure_vncMng()));
    QObject::connect(evMng, SIGNAL(killVncServer()), vncMng, SLOT(killVncServer()));
    QObject::connect(evMng, SIGNAL(killVncServer()), gui, SLOT(killVncServer()));
    QObject::connect(vncMng, SIGNAL(vncAlreadyLaunched(QString)), evMng, SLOT(vncAlreadyLaunched(QString)));

    QObject::connect(vncMng, SIGNAL(vncLaunched(QString)), evMng, SLOT(vncLaunched(QString)));
    QObject::connect(evMng, SIGNAL(show_launched_window(QString)), gui, SLOT(show_launched_window(QString)));
    QObject::connect(gui, SIGNAL(abilitaBackBtn()), evMng, SLOT(abilitaBackBtn()));
    QObject::connect(evMng, SIGNAL(selectNoBtn()), gui, SLOT(selectNoBtn()));
    QObject::connect(evMng, SIGNAL(selectYesBtn()), gui, SLOT(selectYesBtn()));
    QObject::connect(evMng, SIGNAL(show_alert_window(QString)), gui, SLOT(show_alert_window(QString)));
    QObject::connect(gui, SIGNAL(closeApp()), evMng, SLOT(closeApp()));
}


void ThreadsManager::startCustomThreads(){

    /*************************************************************************************/
    //START DEI PROPRI THREADS IN QUEST'AREA

    /*************************************************************************************/
}

void ThreadsManager::destroyCustomThreads(){

    QProcess shell;
    shell.execute("/bin/sh", QStringList() << "-c" << log_dir+"/rilanciaSettingsManager.sh &");
    shell.close();

}
