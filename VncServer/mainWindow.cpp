#include "mainWindow.h"
#include "envheader.h" // INCLUSO SOLO PER IMPORTARE IL DEFINE DEBUG
#include "ui_mainWindow.h"
#include <QDebug>


#if DEBUG
    const QString coloreVisibile = "#7fff00";
#else
    const QString coloreVisibile = "white";
#endif

const int LATENZA_TIMER=3000;


MainWindow::MainWindow(QString /* nome_dir_icone_app */, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect( this, SIGNAL(avviaConfigurazioneIniziale()), this, SLOT(configurazioneInizialeGui()));
    connect(&timer_vnc_started, SIGNAL(timeout()), this, SLOT(show_back_label()));
    connect(&timer_vnc_stopped, SIGNAL(timeout()), this, SLOT(closeAppAfterKill()));
}

MainWindow::~MainWindow()
{
    qDebug() << "Distruttore MainWindow";
    delete ui;
}

void MainWindow::avviaGui(){
    emit avviaConfigurazioneIniziale();
    this->show();
}

void MainWindow::configurazioneInizialeGui(){

    ui->background->hide();
    ui->background->setStyleSheet("QWidget { background-color : black; color : "+coloreVisibile+"; }");

    configuraSchermataPrincipale();

    ui->background->show();
}

void MainWindow::configuraSchermataPrincipale(){

    ui->launched_window->hide();
    ui->launched_window->setStyleSheet("QWidget { background-color : black; color : "+coloreVisibile+"; }");
    ui->back_label->setText("Press BACK");
    ui->launched_label->setText("VNC Server launched!\nConnect your client to:");
    ui->back_label->hide();

    ui->alert_window->hide();
    ui->alert_window->setStyleSheet("QWidget { background-color : black; color : "+coloreVisibile+"; }");
    ui->alert_label->setText("VNC connected to:");
    ui->question_label->setText("Do you want to kill it?");
    ui->yes_label->setStyleSheet("border: 1px solid "+coloreVisibile+";  background-color : "+coloreVisibile+"; color : black; ");
    ui->yes_label->setText("yes");
    ui->no_label->setStyleSheet("border: 1px solid "+coloreVisibile+"; ");
    ui->no_label->setText("no");

}


void MainWindow::show_launched_window(QString vnc_addr){
    ui->vnc_addr->setText(vnc_addr);
    ui->launched_window->show();
    timer_vnc_started.start(LATENZA_TIMER);

}

void MainWindow::show_back_label(){
    timer_vnc_started.stop();
    ui->back_label->show();
    emit abilitaBackBtn();
}


void MainWindow::show_alert_window(QString vnc_addr){
    ui->label_addr->setText(vnc_addr);
    ui->alert_window->show();


}

void MainWindow::killVncServer(){
     ui->label_addr->setText("");
     ui->yes_label->setText("");
     ui->no_label->setText("");
     ui->question_label->setText("");
     ui->no_label->setStyleSheet("border: 0px;  background-color : black; ");
     ui->yes_label->setStyleSheet("border: 0px; background-color : black; ");

     ui->alert_label->setText("VNC server killed!");
     timer_vnc_stopped.start(LATENZA_TIMER-1000);
}

void MainWindow::closeAppAfterKill(){
    timer_vnc_stopped.stop();
    emit closeApp();
}

void MainWindow::selectNoBtn(){
    ui->no_label->setStyleSheet("border: 1px solid "+coloreVisibile+";  background-color : "+coloreVisibile+"; color : black; ");
    ui->yes_label->setStyleSheet("border: 1px solid "+coloreVisibile+"; background-color : black; color : "+coloreVisibile+"; ");
}

void MainWindow::selectYesBtn(){
    ui->yes_label->setStyleSheet("border: 1px solid "+coloreVisibile+";  background-color : "+coloreVisibile+"; color : black; ");
    ui->no_label->setStyleSheet("border: 1px solid "+coloreVisibile+"; background-color : black; color : "+coloreVisibile+"; ");
}
