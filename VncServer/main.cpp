#include "mainWindow.h"
#include "threadsManager.h"
#include <QApplication>
#include <QDebug>

int main(int argc, char *argv[])
{

    if( QString(argv[1]) == "" || QString(argv[2]) == "" ){
        qDebug() << "Inserire parametri: <nome_dir_binario> <nome_dir_icone_app>";
        return 0;
    }
    QString nome_dir_binario(argv[1]);
    QString nome_dir_icone_app(argv[2]);

    QApplication a(argc, argv);

    MainWindow gui(nome_dir_icone_app);

    ThreadsManager threadsMng(nome_dir_binario, nome_dir_icone_app, &gui, qApp );

    return a.exec();

}
