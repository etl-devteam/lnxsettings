#ifndef EVENTSMANAGER_H
#define EVENTSMANAGER_H

#include "envheader.h"
#include <QObject>
#include <QTimer>
#include <QProcess>


class EventsManager : public QObject
{
    Q_OBJECT

    public:
        explicit EventsManager(QObject *parent = nullptr);
        ~EventsManager();
        QString location_sys_class;

    private:
        QTimer *timer;
        int choice_input_key1;      // memorizza il valore del tasto 1
        int choice_input_key2;      // memorizza il valore del tasto 2
        int choice_input_key3;      // memorizza il valore del tasto 3
        int choice_input_key4;      // memorizza il valore del tasto 4
        int old_choice_input_key1;      // memorizza l'ultimo valore del tasto 1
        int old_choice_input_key2;      // memorizza l'ultimo valore del tasto 2
        int old_choice_input_key3;      // memorizza l'ultimo valore del tasto 3
        int old_choice_input_key4;      // memorizza l'ultimo valore del tasto 4
        bool vncMng_configured;
        bool read_back_btn;
        bool vncServerLaunched;   //Variable usata per indicare che l'app è stata lanciata con vnc già in esecuzione
        bool yes_btn;   //true btn yes selezionato, false btn no selezionato

        /***************************************************/

         void identificaEventoTastoRicevuto();
         void letturaTasti();
         void returnToLauncher();
         void chiusuraForzata();
         void accendiLedBlue();

#if !DEBUG
        void previeniCavoStaccato(){
            /*********************************************************************************************/
            // Code for copy the current application in a folder used  for re-launch
            // the closed application in case of release of glasses cable;

            QString cmd_restart = "touch Template /home/dinex/bin/app_running & ";      // Name of application to write in a temporary file contained in a folder ('app_running')
            QProcess shell_restart;
            shell_restart.execute("/bin/sh", QStringList() << "-c" << cmd_restart);
            shell_restart.close();

        }


#endif


    signals:
        void tastoLetto();
        void chiudiApplicazione();
        void abilitaTouchManager();
        void show_launched_window(QString);
        void configure_vncMng();
        void show_alert_window(QString);
        void killVncServer();
        void selectNoBtn();
        void selectYesBtn();


    private slots:
        void task();

    public slots:
        void avviaEventsMng();
        void riabilitaBtns();
        void stopTimer();
        void vncAlreadyLaunched(QString);
        void vncLaunched(QString);
        void abilitaBackBtn();
        void closeApp();


#if USE_TOUCH
        void eventFromTouch(int);
#endif


};

#endif // EVENTSMANAGER_H
