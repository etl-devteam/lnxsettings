#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    public:
        explicit MainWindow(QString nome_dir_icone_app, QWidget *parent = nullptr);
        ~MainWindow();


        void avviaGui();

    private:
        Ui::MainWindow *ui;
        QTimer timer_vnc_started;
        QTimer timer_vnc_stopped;

        /************************************************************************/

         void configuraSchermataPrincipale();



     signals:
         void operazioneCompletata(); // emesso al termine di un operazione richiesta da un btn premuto
         void avviaConfigurazioneIniziale();
         void abilitaBackBtn();
         void closeApp();

    public slots:
        void configurazioneInizialeGui();
        void show_launched_window(QString);
        void show_back_label();
        void show_alert_window(QString);
        void killVncServer();
        void closeAppAfterKill();
        void selectNoBtn();
        void selectYesBtn();

};

#endif // MAINWINDOW_H
