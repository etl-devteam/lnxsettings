#ifndef VNCMANAGER_H
#define VNCMANAGER_H

#include <QObject>

class VncManager : public QObject
{
    Q_OBJECT

    public:
        explicit VncManager(QString vncManagerConfDir, QObject *parent = nullptr);

    private:
        QString vncManagerConfDir;
        QString vnc_port;
        QString ip_addr;
        void genLaunchingScript();
        void readAndSetChosenPort();
        void launchVncServer();
        void addrToShow();



    signals:
        void vncAlreadyLaunched(QString);
        void vncLaunched(QString);




    public slots:
        void killVncServer();
        void configure_vncMng();


};

#endif // VNCMANAGER_H
