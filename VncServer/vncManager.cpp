#include "vncManager.h"
#include "envheader.h"
#include <QProcess>
#include <QFile>
#include <QTextStream>
#include <QHostAddress>
#include <QNetworkInterface>
#include <QDebug>

VncManager::VncManager(QString vncManagerConfDir, QObject *parent) :
    QObject(parent),
    vncManagerConfDir(vncManagerConfDir)
{
    readAndSetChosenPort();
    addrToShow();

}

void VncManager::configure_vncMng(){


    if(QFile::exists(log_dir+"/VncServer/vncserver_pid.log")){
        emit vncAlreadyLaunched(ip_addr+":"+vnc_port);
    }
    else{
        readAndSetChosenPort();
        addrToShow();
        genLaunchingScript();
        launchVncServer();

        emit vncLaunched(ip_addr+":"+vnc_port);

    }
}

void VncManager::readAndSetChosenPort(){

    QFile r_file(vncManagerConfDir+"/vncManager.conf");

    if( r_file.open(QIODevice::ReadOnly | QIODevice::Text)){
        QTextStream in( &r_file );
        vnc_port=in.readLine();
    }

    r_file.close();
}

void VncManager::genLaunchingScript(){


    QFile w_file(log_dir+"/VncServer/Scripts/launchVncServer.sh");
    if(w_file.open(QIODevice::WriteOnly| QIODevice::Text)){
        QTextStream outputFileStream(&w_file);
        outputFileStream << "#!/bin/sh" << endl;
        outputFileStream << endl;
        outputFileStream << "PATH_PID_FILE=$1" << endl;
#if DEBUG
        outputFileStream << "xclock &" << endl;
#else
        outputFileStream << "framebuffer-vncserver -p "+vnc_port+" &" << endl;
#endif
        outputFileStream << "echo $! > $PATH_PID_FILE/vncserver_pid.log" << endl;
    }

    w_file.close();


    QProcess shell;
    shell.execute("/bin/sh", QStringList() << "-c" << "chmod +x "+log_dir+"/VncServer/Scripts/launchVncServer.sh");
    shell.close();


}

void VncManager::launchVncServer(){
    QProcess shell;
    shell.execute("/bin/sh", QStringList() << "-c" << log_dir+"/VncServer/Scripts/launchVncServer.sh "+log_dir+"/VncServer");
    shell.close();

}

void VncManager::killVncServer(){

    QProcess shell;
    shell.execute("/bin/sh", QStringList() << "-c" << "kill -9 $(cat "+log_dir+"/VncServer/vncserver_pid.log)");
    shell.execute("/bin/sh", QStringList() << "-c" << "rm "+log_dir+"/VncServer/vncserver_pid.log");
    shell.close();
}

void VncManager::addrToShow(){
    QList<QHostAddress> list = QNetworkInterface::allAddresses();

     for(int nIter=0; nIter<list.count(); nIter++)
      {
          if(!list[nIter].isLoopback())
                if (list[nIter].protocol() == QAbstractSocket::IPv4Protocol ){
                    ip_addr = list[nIter].toString();
                    break;
                }
      }
}
