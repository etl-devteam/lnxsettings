#include <qglobal.h>
#include <QProcess>
#include <QCoreApplication>

#include "appModel.h"


AppModel::AppModel() :
    AbstractVisionARModel(),
    status    { AppStatus::MAIN_WINDOW },
    currOption{ Option::DISPLAY        }
{
}


AppModel::~AppModel()
{
}


void AppModel::runModel()
{
    emit showWindow(FIRST_WINDOW);
}


void AppModel::backReleased(int /*timePressed*/)
{
    switch (status)
    {
    case AppStatus::MAIN_WINDOW:
        emit newAlert(1, "QuestionCloseApp", "Return to the Launcher?");

        break;

    default:
        break;
    }
}


void AppModel::okPressed()
{
    switch (status)
    {
    case AppStatus::MAIN_WINDOW:

        /* questo codice dovrà essere cambiato
           così da allinearsi con le rispettive app
        */
/*
        switch (currOption)
        {
        case Option::DISPLAY:
            QProcess::execute("/bin/sh", {"-c", SETTINGS_APPS_DIR + QString("/DisplaySettings ") +
                                                SETTINGS_APPS_DIR + QString(' ') +
                                                CONFS_DIR});

            QCoreApplication::instance()->quit();

            break;

        case Option::VNC_SERVER: break;
        case Option::NETWORK_MANAGER: break;
        }
*/
        break;

    default:
        break;
    }
}


void AppModel::forwardPressed()
{
    switch (status)
    {
    case AppStatus::MAIN_WINDOW:
        switch (currOption)
        {
        case Option::DISPLAY: break;

        case Option::VNC_SERVER:
            currOption = Option::DISPLAY;
            emit showTitle("Display");
            emit toggleArrowUp();

            break;

        case Option::NETWORK_MANAGER:
            currOption = Option::VNC_SERVER;
            emit showTitle("VNC Server");
            emit toggleArrowDown();

            break;
        }

        break;

    default:
        break;
    }
}


void AppModel::backwardPressed()
{
    switch (status)
    {
    case AppStatus::MAIN_WINDOW:
        switch (currOption)
        {
        case Option::DISPLAY:
            currOption = Option::VNC_SERVER;
            emit showTitle("VNC Server");
            emit toggleArrowUp();

            break;

        case Option::VNC_SERVER:
            currOption = Option::NETWORK_MANAGER;
            emit showTitle("Network Manager");
            emit toggleArrowDown();

            break;

        case Option::NETWORK_MANAGER: break;
        }

        break;

    default:
        break;
    }
}


void AppModel::manageAlertsEnd(QString id, bool value)
{
    if (status == AppStatus::MAIN_WINDOW &&
            id == "QuestionCloseApp" &&
            value)
    {
        emit endApplication();
    }
}

