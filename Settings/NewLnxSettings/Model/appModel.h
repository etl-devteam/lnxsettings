#ifndef APPMODEL_H
#define APPMODEL_H

#include <VisionAR/Framework/abstractVisionARModel.h>


class AppModel final : public AbstractVisionARModel
{
    Q_OBJECT

public:
    AppModel();
    ~AppModel();

private:

    enum : short {
        FIRST_WINDOW = 1,
    };

    enum class AppStatus : short {
        MAIN_WINDOW,
    };

    enum class Option : short {
        DISPLAY, VNC_SERVER, NETWORK_MANAGER
    };

    static constexpr char const * const SETTINGS_APPS_DIR{"/home/dinex/bin/apps/Settings/SettingsApps"};
    static constexpr char const * const CONFS_DIR        {"/etc/VisionAR/Settings"};

    AppStatus status;
    Option    currOption;

    void runModel() override;

    void backPressed() override {}
    void backReleased(int timePressed) override;

    void okPressed() override;
    void okReleased(int /*timePressed*/ = 0) override {}

    void forwardPressed() override;
    void forwardReleased(int /*timePressed*/) override {}

    void backwardPressed() override;
    void backwardReleased(int /*timePressed*/) override {}

    void singleTap() override {};
    void doubleTap() override {};

    void swipeForward() override  {};
    void swipeBackward() override {};

    void manageAlertsEnd(QString id, bool value) override;

private slots:

signals:
    void showTitle(QString name);
    void toggleArrowUp();
    void toggleArrowDown();
};


#endif // APPMODEL_H
