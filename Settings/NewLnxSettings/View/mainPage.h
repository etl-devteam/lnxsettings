#ifndef MAINPAGE_H
#define MAINPAGE_H

#include "ui_mainWindow.h"


class MainPage final
{
public:
    MainPage();
    ~MainPage();

    void setupUi(QWidget *widget);

    void showTitle(QString name);
    void toggleArrowUp();
    void toggleArrowDown();

private:
    Ui_mainPage *mainPage;
};


#endif // MAINPAGE_H
