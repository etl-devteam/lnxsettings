#include "appView.h"


AppView::AppView() :
    AbstractVisionARView(),
    mainPage{ new MainPage }
{
    // the order in which you "register window"s here
    // matches the number you pass in "showWindow(int)"

    registerWindow(mainPage.data());
}


void AppView::showTitle(QString name)
{
    mainPage->showTitle(name);
}


void AppView::toggleArrowDown()
{
    mainPage->toggleArrowDown();
}


void AppView::toggleArrowUp()
{
    mainPage->toggleArrowUp();
}
