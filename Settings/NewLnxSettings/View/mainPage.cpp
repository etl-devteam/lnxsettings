#include "mainPage.h"


MainPage::MainPage() :
    mainPage{ new Ui_mainPage }
{
}


MainPage::~MainPage()
{
    delete mainPage;
    mainPage = nullptr;
}


void MainPage::setupUi(QWidget *widget)
{
    mainPage->setupUi(widget);

    mainPage->labelArrowUp->hide();
}


void MainPage::showTitle(QString name)
{
    mainPage->labelOptionName->setText(name);

    if (name == "Display")
    {
        mainPage->labelIconLeftSide->setPixmap(QPixmap(":/Images/display.png").scaled(mainPage->labelIconLeftSide->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
    }
    else if (name == "VNC Server")
    {
        mainPage->labelIconLeftSide->setPixmap(QPixmap(":/Images/vnc.png").scaled(mainPage->labelIconLeftSide->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
    }
    else
    {
        mainPage->labelIconLeftSide->setPixmap(QPixmap(":/Images/wifi_logo.png").scaled(mainPage->labelIconLeftSide->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
    }
}


void MainPage::toggleArrowDown()
{
    if (mainPage->labelArrowDown->isHidden())
    {
        mainPage->labelArrowDown->show();
    }
    else
    {
        mainPage->labelArrowDown->hide();
    }
}


void MainPage::toggleArrowUp()
{
    if (mainPage->labelArrowUp->isHidden())
    {
        mainPage->labelArrowUp->show();
    }
    else
    {
        mainPage->labelArrowUp->hide();
    }
}
