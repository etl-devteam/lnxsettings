#ifndef APPVIEW_H
#define APPVIEW_H

#include <VisionAR/Framework/abstractVisionARView.h>

#include "mainPage.h"


class AppView final : public AbstractVisionARView
{
    Q_OBJECT

public:
    AppView();

private:
    QScopedPointer<MainPage> mainPage;

public slots:
    void showTitle(QString name);
    void toggleArrowUp();
    void toggleArrowDown();

signals:
};


#endif  // APPVIEW_H
