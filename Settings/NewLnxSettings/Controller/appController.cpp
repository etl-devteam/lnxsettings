#include "appController.h"


AppController::AppController(AppView *view, AppModel *model, QString app_relative_path) :
    AbstractVisionARController(view, model, app_relative_path)
{
    connect((AppModel*)model, SIGNAL(toggleArrowUp()),   (AppView*)view, SLOT(toggleArrowUp()));
    connect((AppModel*)model, SIGNAL(toggleArrowDown()), (AppView*)view, SLOT(toggleArrowDown()));

    connect((AppModel*)model, SIGNAL(showTitle(QString)), (AppView*)view, SLOT(showTitle(QString)));
}


void AppController::connections()
{
}


void AppController::endActions()
{
}
