#!/bin/bash

DESKTOP_DIR=""

if [ "$1" == "1"  ] #PC italiano
then
	DESKTOP_DIR="Scrivania"

elif [ "$1" == "2" ] #PC inglese
then
	DESKTOP_DIR="Desktop"

else
	echo "Errore: parametri errati o mancanti."
	echo "Settare 1 se nella vostra home avete una directory chiamata Scrivania"
	echo "Settare 2 se nella vostra home avete una directory chiamata Desktop"
	exit 0
fi


if [ ! -d "$HOME/$DESKTOP_DIR" ]
then
	echo "Errore: la dir settata per parametro non esiste"
	exit 0
fi

APP_NAME=${PWD##*/}
APPS_CU_LOCATION="$HOME/$DESKTOP_DIR/VisionAR Filesystem/home/dinex/bin/apps" 

if [ ! -d "$HOME/$DESKTOP_DIR/VisionAR Filesystem/home/dinex/bin/apps/$APP_NAME" ]
then
	sudo mkdir "$HOME/$DESKTOP_DIR/VisionAR Filesystem/home/dinex/bin/apps/$APP_NAME"
	sudo echo "#!/bin/sh" > "$HOME/$DESKTOP_DIR/VisionAR Filesystem/home/dinex/bin/apps/$APP_NAME/launch.sh"
	sudo echo "" >> "$HOME/$DESKTOP_DIR/VisionAR Filesystem/home/dinex/bin/apps/$APP_NAME/launch.sh"
	sudo echo "./$APP_NAME /home/dinex/bin/apps/$APP_NAME /etc/eyetechlab/$APP_NAME/icons" >> "$HOME/$DESKTOP_DIR/VisionAR Filesystem/home/dinex/bin/apps/$APP_NAME/launch.sh"
		
	sudo chmod +x "$HOME/$DESKTOP_DIR/VisionAR Filesystem/home/dinex/bin/apps/$APP_NAME/launch.sh"
fi


sudo cp "bin_cu/$APP_NAME" "$HOME/$DESKTOP_DIR/VisionAR Filesystem/home/dinex/bin/apps/$APP_NAME"





