#ifndef SETTINGSMANAGER_H
#define SETTINGSMANAGER_H

#include <QObject>
#include "appObj.h"

class SettingsManager : public QObject
{
    Q_OBJECT

    public:
        explicit SettingsManager( QString app_dir,
                                  QString icons_dir,
                                  QString default_apps_path,
                                  QString configs_file_path,
                                  QObject *parent = nullptr);

        ~SettingsManager();

    private:
        QString default_apps_path;
        QString configs_file_path;
        QList<AppObj*> info_apps_list;
        int indice_last_selected_app;

        /*******************************************************************/

        void generaScriptRilanciaSettingsMng( QString app_dir, QString icons_dir );

        void stampaAppsRilevate();

        void creaAppObj( QString directory, QString name_bin,
                         QString path, QString icon_name,
                         QString icons_dir, QString name_to_show);

        bool leggiOpzione( QString nome_opzione, QString valore_opzione,
                           QString break_star, QString nome_file_conf,
                           QHash<QString, QString>  *listaOpzioni,
                           bool *confFileValido ); // ritorna true se l'opzione letta e' ok

        void parseConfFile(QString nome_file);

        int calcFrecetteType();

        void stampaInfoAppsList();

    signals:
        void selezionaApplicazione(QString name_to_show, QString icon_name, int posizione);
        void applicazioneSingola(); // inviato solo in caso sia presente una sola app per togliere le freccie
        void btnNotValid();
        void terminaSettingManager();

    public slots:
        void caricaApplicazioni();
        void incrementa_last_selected_app();
        void decrementa_last_selected_app();
        void lanciaApplicazioneSelezionata();

};

#endif // SETTINGSMANAGER_H
