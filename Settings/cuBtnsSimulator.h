#ifndef CUBTNSSIMULATOR_H
#define CUBTNSSIMULATOR_H

#include <QObject>
#include <QTimer>
/***************************/
#include <sys/types.h>
#include <linux/hdreg.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <qplatformdefs.h>
#include "linux/input.h"



class CUBtnsSimulator : public QObject
{
    Q_OBJECT



    public:
        explicit CUBtnsSimulator(QString device, QString location_sys_class, QObject *parent = nullptr);
        ~CUBtnsSimulator();

    private:
        QString device;
        int fd;
        struct input_event ev[1]; //dove vengono memorizzati i codici dei tasti premuti
        int size;
        bool btns_abilitati;
        bool device_connection_closed;
        bool second_read; //non so come mai ma ogni bottone premuto invia due volte lo stesso evento tale flag evita che il secondo evento venga preso
        QTimer polling_timer;
        QString location_sys_class;

    signals:

    private slots:
        void ascoltaBtns();

    public slots:
        void avviaSimulator();
        void abilitaLetturaTasti();
        void azzera_input_keys();
        void stopTimer();

};

#endif // CUBTNSSIMULATOR_H
