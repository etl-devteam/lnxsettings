#ifndef APPOBJ_H
#define APPOBJ_H

#include <QObject>

class AppObj : public QObject
{
    Q_OBJECT

    public:
        explicit AppObj(QString path, QString directory, QString name_bin,
                        QString icon_name, QString icons_dir, QString name_to_show, QObject *parent = nullptr);

        QString path;
        QString directory;
        QString name_bin;
        QString icon_name;
        QString icons_dir;
        QString name_to_show;

    signals:

    public slots:

};

#endif // APPOBJ_H
