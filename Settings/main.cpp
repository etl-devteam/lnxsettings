#include "mainWindow.h"
#include "threadsManager.h"
#include <QApplication>
#include <QDebug>
#include "envheader.h"


int main(int argc, char *argv[])
{

    QString nome_dir_binario((argc>1)?argv[1]:(apps_dir+"/Settings"));
    QString nome_dir_icone_app((argc>2)?argv[2]:(cfg_dir+"/Settings/icons"));

    QApplication a(argc, argv);

    /* Tolgo il cursore del mouse */
    QCursor cursor(Qt::BlankCursor);
    QApplication::setOverrideCursor(cursor);
    QApplication::changeOverrideCursor(cursor);

    MainWindow gui(nome_dir_icone_app);

    ThreadsManager threadsMng(nome_dir_binario, nome_dir_icone_app, &gui, qApp );

    return a.exec();

}
