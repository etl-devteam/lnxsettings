#ifndef THREADSMANAGER_H
#define THREADSMANAGER_H

#include <QObject>
#include <QApplication>
#include <QThread>

#include "envheader.h"
#include "mainWindow.h"
#include "eventsManager.h"
#include "cuBtnsSimulator.h"
#include "touchManager.h"
#include "settingsManager.h"

class ThreadsManager : public QObject
{
    Q_OBJECT

    public:
        explicit ThreadsManager(MainWindow *gui,
                                QApplication *qAppl,
                                QObject *parent = nullptr);
        explicit ThreadsManager(QString nome_dir_binario,
                                QString nome_dir_icone_app,
                                MainWindow *gui,
                                QApplication *qAppl,
                                QObject *parent = nullptr);

    private:
        MainWindow *gui;
        QApplication *qAppl;
        QThread *threadSettingsMng, *threadEvMng;
        EventsManager *evMng;
        SettingsManager *settingsMng;


#if DEBUG
        QThread *threadCubSim;
        CUBtnsSimulator *cubSim;
#endif

#if USE_TOUCH
        QThread *threadTouchMng;
        TouchManager *touchMng;
#endif

    /*************************************************************************************/
    // Usare questi metodi per personalizzare costruttore, lancio e chiusura app.
    // Lasciare invariati costruttore, startApplication(), chiudiApplicazione();

        void inizializzazioneCustom();
        void startCustomThreads();
        void destroyCustomThreads();

    /*************************************************************************************/

        void startApplication();

    signals:
        void stopTimer();

    public slots:
        void chiudiApplicazione();

};

#endif // THREADSMANAGER_H
