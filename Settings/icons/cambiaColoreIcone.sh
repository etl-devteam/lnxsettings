#!/bin/bash

COLOR_TO_SET="#7fff00"
COLOR_TO_CHANGE="white"

if [ "$3" == "inverti" ]
then
	COLOR_TO_CHANGE="#7fff00"
	COLOR_TO_SET="white"
fi

if [ "$1" == "-d" ] # se viene specificata una directory
then
	DIR_NAME="$2"

	if [ -d "$DIR_NAME" ]
	then
		for FILE in $(ls $DIR_NAME/*.png)
		do
			echo "Trovato file: $FILE"
			
			if [ -f "$FILE" ]
			then
				convert "$FILE"  -fuzz XX% -fill "$COLOR_TO_SET" -opaque "$COLOR_TO_CHANGE" "$FILE"
			else
				echo "Errore: specificare un immagine esistente"
			fi
		done
	else
		echo "Errore: directory specificata inesistente"
		exit 0
	fi

elif [ "$1" == "-f" ]
then
	FILE="$2"
	
    if [ -f "$FILE" ]
    then
        convert "$FILE"  -fuzz XX% -fill "$COLOR_TO_SET" -opaque "$COLOR_TO_CHANGE" "$FILE"
    else
        echo "Errore: specificare un immagine esistente"
    fi

else

	echo "Errore: parametri sconosciuti"
fi	


