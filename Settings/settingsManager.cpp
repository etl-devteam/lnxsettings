#include "settingsManager.h"
#include "envheader.h"
#include <QProcess>
#include <QFile>
#include <QTextStream>
#include <QDebug>

SettingsManager::SettingsManager(QString app_dir,
                                 QString icons_dir,
                                 QString default_apps_path,
                                 QString configs_file_path,
                                 QObject *parent) :
    QObject(parent),
    default_apps_path(default_apps_path),
    configs_file_path(configs_file_path),
    info_apps_list(),
    indice_last_selected_app(0)
{
        generaScriptRilanciaSettingsMng( app_dir, icons_dir );
}

SettingsManager::~SettingsManager(){
    qDebug() << "Distruttore SettingsManager";
    for( AppObj *app : info_apps_list )
    {

        delete app;


    }
}

void SettingsManager::creaAppObj(QString directory, QString name_bin, QString path, QString icon_name, QString icons_dir, QString name_to_show){
    AppObj *tmp = new AppObj(path, directory, name_bin,
                             icon_name, icons_dir, name_to_show);

    qDebug() << "Apps creata col nome "+name_bin;
    info_apps_list.push_back(tmp);


}

bool SettingsManager::leggiOpzione( QString nome_opzione, QString valore_opzione,
                                       QString break_star,  QString nome_file_conf,
                                            QHash<QString, QString>  *listaOpzioni, bool *confFileValido ){

    if( break_star != "*"){ // verifico che venga rispettata la sintassi dell'asterisco
        *confFileValido = false;
        return false;   //sintassi errata

    }

    if( nome_opzione == "path:" ){
        if( valore_opzione == "default" ){
            listaOpzioni->insert("path", default_apps_path);

        }
        else{
            listaOpzioni->insert("path", valore_opzione);

        }

    }
    else if( nome_opzione == "dir_name:" ){

        if( valore_opzione == "default" ){
            listaOpzioni->insert("dir_name", nome_file_conf.split(".")[0]);

        }
        else{
            listaOpzioni->insert("dir_name", valore_opzione);

        }

    }
    else if( nome_opzione == "app_name:" ){

        if( valore_opzione == "default" ){
            listaOpzioni->insert("app_name", nome_file_conf.split(".")[0]);

        }
        else{
            listaOpzioni->insert("app_name", valore_opzione);

        }

    }
    else if( nome_opzione == "icon_name:" ){

        if( valore_opzione == "default" ){
            listaOpzioni->insert("icon_name", configs_file_path+"/../icons/icon.png");

        }
        else{
            listaOpzioni->insert("icon_name", valore_opzione);

        }

    }
    else if( nome_opzione == "icons_dir:" ){

        if( valore_opzione == "default" ){
            listaOpzioni->insert("icons_dir", configs_file_path+"/../../"+listaOpzioni->value("app_name") +"/icons");

        }
        else{
            listaOpzioni->insert("icons_dir", valore_opzione);

        }

    }
    else if( nome_opzione == "name_to_show:" ){

        if( valore_opzione == "default" ){
            listaOpzioni->insert("name_to_show", nome_file_conf.split(".")[0]);
        }
        else{
            listaOpzioni->insert("name_to_show", valore_opzione);
        }

    }
    else{   // se viene trovata un'opzione sconosciuta il file non e' valido
        *confFileValido = false;
        return false;   // opzione sconosciuta
    }

    return true; // tutto ok

}

void SettingsManager::parseConfFile(QString nome_file){

    bool confFileValido = true;
    bool startLetturaSezioneConf = false;
    bool endLetturaSezioneConf = false;
    QHash<QString, QString> opzioniConfFile;

    QFile confFile(nome_file);    //apro il file di configurazione
    if( confFile.open(QIODevice::ReadOnly| QIODevice::Text) ){

        QTextStream stream_confFile( &confFile );
        while(!stream_confFile.atEnd() && confFileValido){
            QString line0 = stream_confFile.readLine();

            if(!startLetturaSezioneConf){ //ignora tutto cio' che c'e' prima della quadra aperta
                if( line0 == "[" )
                    startLetturaSezioneConf = true;
            }
            else if(!endLetturaSezioneConf){
                if( line0 == "]" ){
                    endLetturaSezioneConf = true;
                }
                else{   // leggo le varie opzioni;

                    QString line1 = stream_confFile.readLine();
                    QString line2 = stream_confFile.readLine();

                    if( !leggiOpzione(line0.trimmed(), line1.trimmed(),
                                      line2.trimmed(), nome_file.split("/").last(),
                                      &opzioniConfFile, &confFileValido ) ){

                        qDebug() << nome_file + ": Sintassi errata! " +confFileValido;
                    }

                }
            }

        }
    }

    confFile.close();

    if( confFileValido ){
        creaAppObj(opzioniConfFile.value("dir_name"), opzioniConfFile.value("app_name"),
                    opzioniConfFile.value("path"), opzioniConfFile.value("icon_name"),
                        opzioniConfFile.value("icons_dir"), opzioniConfFile.value("name_to_show"));
    }

}


void SettingsManager::caricaApplicazioni(){

    QProcess shell;
    shell.execute("/bin/sh",  QStringList() << "-c" << "ls "+configs_file_path+"/*.conf > "+log_dir+"/settings.log");
    shell.close();

    QFile apps_rilevate(log_dir+"/settings.log");    //dentro settings.log vi e' l'elenco dei file di configurazione
    if( apps_rilevate.open(QIODevice::ReadOnly| QIODevice::Text) ){

        QTextStream stream_settings( &apps_rilevate );
        while(!stream_settings.atEnd()){

           QString fileAppConf = stream_settings.readLine();

           parseConfFile(fileAppConf);

        }
        apps_rilevate.close();

        stampaAppsRilevate(); // debug

        if( !info_apps_list.isEmpty() )
            emit selezionaApplicazione(info_apps_list[0]->name_to_show,
                                        info_apps_list[0]->icon_name,
                                            calcFrecetteType());

        if( info_apps_list.count() == 1 )
            emit applicazioneSingola();
    }
    else{
        qDebug() << "Setting.log: File non trovato";
    }


}

void SettingsManager::stampaAppsRilevate(){
    for( AppObj *app : info_apps_list ){
        qDebug() << "";
        qDebug() << "################ App rilevata ################";
        qDebug() << "Path: " << app->path;
        qDebug() << "Dirname: " << app->directory;
        qDebug() << "Name_bin: " << app->name_bin;
        qDebug() << "Icons_dir: " << app->icons_dir;
        qDebug() << "Icon_name: " << app->icon_name;
        qDebug() << "Name_to_show: " << app->name_to_show;
        qDebug() << "##############################################";
        qDebug() << "";
    }
}

int SettingsManager::calcFrecetteType(){
    if( indice_last_selected_app == 0 ){
        return 1;
    }
    else if( indice_last_selected_app == info_apps_list.count()-1 ){
        return 3;
    }

    return 2;

}

void SettingsManager::incrementa_last_selected_app(){
    if( indice_last_selected_app < info_apps_list.count()-1 ){
        indice_last_selected_app++;

        emit emit selezionaApplicazione(info_apps_list[indice_last_selected_app]->name_to_show,
                                        info_apps_list[indice_last_selected_app]->icon_name,
                                        calcFrecetteType());
    }
    else{
        emit btnNotValid();
    }

}

void SettingsManager::decrementa_last_selected_app(){
    if( indice_last_selected_app > 0 ){
        indice_last_selected_app--;
        emit emit selezionaApplicazione(info_apps_list[indice_last_selected_app]->name_to_show,
                                        info_apps_list[indice_last_selected_app]->icon_name,
                                        calcFrecetteType());
    }
    else{
         emit btnNotValid();
    }

}

void SettingsManager::generaScriptRilanciaSettingsMng( QString app_dir, QString icons_dir){
    QProcess shell;
    shell.execute("/bin/sh",  QStringList() << "-c" << "rm "+log_dir+"/rilanciaSettingsManager.sh");


    QFile script_rilanciaSettingsMng(log_dir+"/rilanciaSettingsManager.sh");
    if (script_rilanciaSettingsMng.open(QIODevice::ReadWrite)) {
        QTextStream stream(&script_rilanciaSettingsMng);
#if DEBUG
        stream << "sudo "+app_dir+"/Settings "+app_dir+" "+icons_dir;
#else
       stream << app_dir+"/Settings "+app_dir+" "+icons_dir;
#endif

    }

    script_rilanciaSettingsMng.close();


    shell.execute("/bin/sh",  QStringList() << "-c" << "chmod +x "+log_dir+"/rilanciaSettingsManager.sh");
    shell.close();

}


void SettingsManager::lanciaApplicazioneSelezionata(){
    stampaInfoAppsList();

    QString absolutePathApp = info_apps_list[indice_last_selected_app]->path;
    absolutePathApp += "/" + info_apps_list[indice_last_selected_app]->directory;
    absolutePathApp += "/" + info_apps_list[indice_last_selected_app]->name_bin;

    QProcess shell;
#if DEBUG
    shell.execute("/bin/sh",  QStringList() << "-c" << "echo akronos | sudo -S "+absolutePathApp + " " + info_apps_list[indice_last_selected_app]->path + info_apps_list[indice_last_selected_app]->directory + " " + info_apps_list[indice_last_selected_app]->path +"/"+ info_apps_list[indice_last_selected_app]->directory + "/../icons &");
#else
    qDebug() << "Sto per eseguire:" << endl;
    qDebug() << absolutePathApp + " " + info_apps_list[indice_last_selected_app]->path + "/" + info_apps_list[indice_last_selected_app]->directory + " " + info_apps_list[indice_last_selected_app]->icons_dir+" &";
    shell.execute("/bin/sh",  QStringList() << "-c" << absolutePathApp + " " + info_apps_list[indice_last_selected_app]->path + "/" + info_apps_list[indice_last_selected_app]->directory + " " + info_apps_list[indice_last_selected_app]->icons_dir+" &");
#endif

    shell.close();

    emit terminaSettingManager();
}

void SettingsManager::stampaInfoAppsList(){
    for( AppObj *app : info_apps_list )
    {
        qDebug() << "-----------------------------------------";
        qDebug() << "SettingManager contengo: "+app->name_bin;
        qDebug() << "-----------------------------------------";

    }
}

