#include "threadsManager.h"

ThreadsManager::ThreadsManager(QString nome_dir_binario,
                               QString nome_dir_icone_app,
                               MainWindow *gui,
                               QApplication *qAppl,
                               QObject *parent) :
    QObject(parent),
    gui(gui),
    qAppl(qAppl),
    threadSettingsMng(new QThread()),
    threadEvMng(new QThread()),
    evMng(new EventsManager()),
    settingsMng(new SettingsManager(nome_dir_binario,
                                    nome_dir_icone_app,
                                    nome_dir_binario+"/..",
                                    cfg_dir+"/Settings/confs"))
{
    //Can't touch this (oh-oh oh oh oh-oh-oh)

    inizializzazioneCustom();


    /*************************************************************************************/

    evMng->moveToThread(threadEvMng);

    QObject::connect(threadEvMng, SIGNAL(started()), evMng, SLOT(avviaEventsMng()));
    QObject::connect(threadEvMng, SIGNAL(finished()), evMng, SLOT(deleteLater()));
    QObject::connect(gui, SIGNAL(operazioneCompletata()), evMng, SLOT(riabilitaBtns()));
    QObject::connect(evMng, SIGNAL(chiudiApplicazione()), this, SLOT(chiudiApplicazione()));
    QObject::connect(this, SIGNAL(stopTimer()), evMng, SLOT(stopTimer()));

    /*************************************************************************************/

#if DEBUG
    cubSim= new CUBtnsSimulator(CU_FILE_DEVICE, evMng->location_sys_class);
    threadCubSim = new QThread();

    cubSim->moveToThread(threadCubSim);
    QObject::connect(threadCubSim, SIGNAL(started()), cubSim, SLOT(avviaSimulator()));
    QObject::connect(threadCubSim, SIGNAL(finished()), cubSim, SLOT(deleteLater()));
    QObject::connect(evMng, SIGNAL(tastoLetto()), cubSim, SLOT(azzera_input_keys()));
    QObject::connect(this, SIGNAL(stopTimer()), cubSim, SLOT(stopTimer()));


#endif

#if USE_TOUCH
    touchMng = new TouchManager(TOUCH_FILE_DEVICE);
    threadTouchMng = new QThread();

    touchMng->moveToThread(threadTouchMng);

    QObject::connect( threadTouchMng, SIGNAL(started()), touchMng, SLOT(avviaTouchManager()));
    QObject::connect( threadTouchMng, SIGNAL(finished()), touchMng, SLOT(deleteLater()));
    QObject::connect( evMng, SIGNAL(abilitaTouchManager()), touchMng, SLOT(abilitaTouch()));
    QObject::connect( touchMng, SIGNAL(tastoPremuto(int)), evMng, SLOT(eventFromTouch(int)));
    QObject::connect(this, SIGNAL(stopTimer()), touchMng, SLOT(stopTimer()));


#endif

    startApplication();

}

void ThreadsManager::startApplication(){
    gui->avviaGui();

#if DEBUG
    threadCubSim->start();
#endif

#if USE_TOUCH
    threadTouchMng->start();
#endif

    threadEvMng->start();

    startCustomThreads();
}


void ThreadsManager::chiudiApplicazione(){
    //Can't touch this (oh-oh oh oh oh-oh-oh)

     emit stopTimer();

#if DEBUG

    threadCubSim->quit();
    threadCubSim->wait();

    delete  threadCubSim;
#endif

#if USE_TOUCH

    threadTouchMng->quit();
    threadTouchMng->wait();

    delete  threadTouchMng;
#endif

    threadEvMng->quit();
    threadEvMng->wait();

    delete threadEvMng;


    destroyCustomThreads();

    qAppl->exit();
}


void ThreadsManager::inizializzazioneCustom(){

    settingsMng->moveToThread(threadSettingsMng);

    QObject::connect(threadSettingsMng, SIGNAL(finished()), settingsMng, SLOT(deleteLater()));
    QObject::connect(settingsMng, SIGNAL(selezionaApplicazione(QString, QString, int)), gui, SLOT(selezionaApplicazione(QString, QString, int)));
    QObject::connect(settingsMng, SIGNAL(applicazioneSingola()), gui, SLOT(togliFreccie()));
    QObject::connect(settingsMng, SIGNAL(btnNotValid()), evMng, SLOT(riabilitaBtns()));
    QObject::connect(settingsMng, SIGNAL(terminaSettingManager()), this, SLOT(chiudiApplicazione()));
    QObject::connect(evMng, SIGNAL(caricaApplicazioni()), settingsMng, SLOT(caricaApplicazioni()));
    QObject::connect(evMng, SIGNAL(incrementa_last_selected_app()), settingsMng, SLOT(incrementa_last_selected_app()));
    QObject::connect(evMng, SIGNAL(decrementa_last_selected_app()), settingsMng, SLOT(decrementa_last_selected_app()));
    QObject::connect(evMng, SIGNAL(lanciaApplicazioneSelezionata()), settingsMng, SLOT(lanciaApplicazioneSelezionata()));

}

void ThreadsManager::startCustomThreads(){

    threadSettingsMng->start();

}

void ThreadsManager::destroyCustomThreads(){

    threadSettingsMng->quit();
    threadSettingsMng->wait();

    delete threadSettingsMng;

}
