#include "cuBtnsSimulator.h"
#include <QDebug>
#include <QProcess>

const int POLLING_FREQUENCY=10;

CUBtnsSimulator::CUBtnsSimulator(QString device, QString location_sys_class, QObject *parent) :
    QObject(parent),
    device(device),
    fd(0),
    size(sizeof(struct input_event)),
    btns_abilitati(true),
    device_connection_closed(true),
    second_read(false),
    polling_timer(this),
    location_sys_class(location_sys_class)
{
    QProcess shell;
    shell.execute("/bin/sh",  QStringList() << "-c" << "echo 0 > "+location_sys_class+"/sys/class/dinema/input_key1");
    shell.execute("/bin/sh",  QStringList() << "-c" << "echo 0 > "+location_sys_class+"/sys/class/dinema/input_key2");
    shell.execute("/bin/sh",  QStringList() << "-c" << "echo 0 > "+location_sys_class+"/sys/class/dinema/input_key3");
    shell.execute("/bin/sh",  QStringList() << "-c" << "echo 0 > "+location_sys_class+"/sys/class/dinema/input_key4");
    shell.close();

    /**************** Connecting timer to private slot ascoltaBtns() ******************************/

    connect(&polling_timer, SIGNAL(timeout()), this, SLOT(ascoltaBtns()));

    /***********************************************************************************************/
}

CUBtnsSimulator::~CUBtnsSimulator(){
    qDebug() << "Distruttore CUBtnsSimulator";
}

void CUBtnsSimulator::stopTimer(){
    qDebug() << "CUBtnsSimulator: stopping timer...";
    polling_timer.stop();
}

void CUBtnsSimulator::avviaSimulator(){
    polling_timer.start(POLLING_FREQUENCY);
}

void CUBtnsSimulator::abilitaLetturaTasti(){
     btns_abilitati = true;
     qDebug() << "CUBtnsSimulator: lettura tasto riabilitata.";
}

void CUBtnsSimulator::ascoltaBtns(){
// Ogni volta che avviene un evento viene prima inviato il codice 4,
// poi il codice del tasto premuto e infine il codice 0;

    if(btns_abilitati){
        if( device_connection_closed ){
            fd = QT_OPEN(device.toLocal8Bit().constData(), O_RDONLY | O_NDELAY, 0);

            if(fd >= 0)
            {
                 // Gain exclusive access to the input_event file
            //    ::ioctl(fd, EVIOCGRAB, 1);
                device_connection_closed = false;
            }

        }



        // Shouldn't happen, but you never know
        if (( QT_READ(fd, ev, size)) < size) {
            return;
        }

        int codiceTasto = 0;
        if( ev[0].code == 4 ){
            // Shouldn't happen, but you never know
            if (( QT_READ(fd, ev, size)) < size) {
                return;
            }

            codiceTasto = ev[0].code;

            if (( QT_READ(fd, ev, size)) < size) { //Leggo l'evento 0
                return;
            }
        }

        if( second_read ){
            second_read = false;
            return;
        }
        else{
            second_read = true;
        }

        if( codiceTasto == 2 || codiceTasto == 3 || codiceTasto == 4 || codiceTasto == 5 ) // se premo i tasti riservati al TouchManager li ignoro
        {
            qDebug() << "Event from CUBtnsSimulator: #"+QString::number(codiceTasto);

            QProcess shell;

            if( codiceTasto == 2 ) // se premo il tasto 1 della tastiera
            {
                btns_abilitati = false;
                QT_CLOSE(fd);
                device_connection_closed = true;
                shell.execute("/bin/sh",  QStringList() << "-c" << "echo 1 > "+location_sys_class+"/sys/class/dinema/input_key1");
            }
            else if( codiceTasto == 3 )
            {
                btns_abilitati = false;
                QT_CLOSE(fd);
                device_connection_closed = true;
                shell.execute("/bin/sh",  QStringList() << "-c" << "echo 1 > "+location_sys_class+"/sys/class/dinema/input_key2");
            }
            else if( codiceTasto == 4 )
            {
                btns_abilitati = false;
                QT_CLOSE(fd);
                device_connection_closed = true;
                shell.execute("/bin/sh",  QStringList() << "-c" << "echo 1 > "+location_sys_class+"/sys/class/dinema/input_key3");
            }
            else if( codiceTasto == 5 )
            {
                btns_abilitati = false;
                QT_CLOSE(fd);
                device_connection_closed = true;
                shell.execute("/bin/sh",  QStringList() << "-c" << "echo 1 > "+location_sys_class+"/sys/class/dinema/input_key4");
            }

            shell.close();

        }


    }
}


void CUBtnsSimulator::azzera_input_keys(){
    QProcess shell;
    shell.execute("/bin/sh",  QStringList() << "-c" << "echo 0 > "+location_sys_class+"/sys/class/dinema/input_key1");
    shell.execute("/bin/sh",  QStringList() << "-c" << "echo 0 > "+location_sys_class+"/sys/class/dinema/input_key2");
    shell.execute("/bin/sh",  QStringList() << "-c" << "echo 0 > "+location_sys_class+"/sys/class/dinema/input_key3");
    shell.execute("/bin/sh",  QStringList() << "-c" << "echo 0 > "+location_sys_class+"/sys/class/dinema/input_key4");
    shell.close();

    abilitaLetturaTasti();
}
