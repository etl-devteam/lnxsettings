#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    public:
        explicit MainWindow(QString nome_dir_icone_app, QWidget *parent = nullptr);
        ~MainWindow();


        void avviaGui();

    private:
        Ui::MainWindow *ui;

        QPixmap img_arrow_up;
        QPixmap img_arrow_down;
        QPixmap icon_selected_app;
        QTimer *blinking_arrow_timer;
        bool arrow_status;      // se e' a true le freccia sono mostrate
        bool single_app;    // true solo nel caso sia registrata una sola app

        /************************************************************************/

         void configuraSchermataPrincipale();


     signals:
         void operazioneCompletata(); // emesso al termine di un operazione richiesta da un btn premuto
         void avviaConfigurazioneIniziale();

    private slots:
        void configurazioneInizialeGui();
        void hideShowArrows();

    public slots:
        void selezionaApplicazione(QString nome_app, QString path_iconaint, int app_position );
        void togliFreccie();



};

#endif // MAINWINDOW_H
