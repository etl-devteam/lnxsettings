#include "appObj.h"

AppObj::AppObj(QString path, QString directory, QString name_bin,
               QString icon_name, QString icons_dir, QString name_to_show, QObject *parent) :
    QObject(parent),
    path(path),
    directory(directory),
    name_bin(name_bin),
    icon_name(icon_name),
    icons_dir(icons_dir),
    name_to_show(name_to_show)
{

}
