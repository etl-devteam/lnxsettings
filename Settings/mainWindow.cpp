#include "mainWindow.h"
#include "envheader.h" // INCLUSO SOLO PER IMPORTARE IL DEFINE DEBUG
#include "ui_mainWindow.h"
#include <QTimer>
#include <QDebug>


#if DEBUG
    const QString coloreVisibile = "#7fff00";
#else
    const QString coloreVisibile = "white";
#endif

const int LATENZA_BLINK=700;

MainWindow::MainWindow(QString nome_dir_icone_app, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    img_arrow_up(nome_dir_icone_app+"/arrow_up.png"),
    img_arrow_down(nome_dir_icone_app+"/arrow_down.png"),
    blinking_arrow_timer(new QTimer(this)),
    arrow_status(false),
    single_app(false)
{
    ui->setupUi(this);

    /**************************************************************************/

    connect( blinking_arrow_timer, SIGNAL(timeout()), this, SLOT(hideShowArrows()));
    connect( this, SIGNAL(avviaConfigurazioneIniziale()), this, SLOT(configurazioneInizialeGui()));

}

MainWindow::~MainWindow()
{
    qDebug() << "Distruttore MainWindow";
    blinking_arrow_timer->stop();
    delete blinking_arrow_timer;
    delete ui;
}

void MainWindow::avviaGui(){
    emit avviaConfigurazioneIniziale();
    this->show();
}

void MainWindow::configurazioneInizialeGui(){

    ui->background->hide();
    ui->background->setStyleSheet("QWidget { background-color : black; color : "+coloreVisibile+"; }");

    configuraSchermataPrincipale();

    ui->background->show();
}

void MainWindow::configuraSchermataPrincipale(){
    ui->up_arrow->setPixmap(img_arrow_up.scaled(ui->up_arrow->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
    ui->down_arrow->setPixmap(img_arrow_down.scaled(ui->down_arrow->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
    ui->option_window->setStyleSheet("background-color : black; color : "+coloreVisibile+";");
    ui->titolo->setStyleSheet("color: "+coloreVisibile+"; font-weight: bold;");
    ui->titolo->setText("Settings");
    QFont font("times", 11);
    ui->titolo->setFont(font);
    ui->icon_app->setText("");
    ui->name_app->setText("");
    ui->window_freccette->hide();
}

void MainWindow::hideShowArrows(){

    if( arrow_status ){
        arrow_status = false;
        ui->window_freccette->hide();
    }
    else{
        arrow_status = true;
        ui->window_freccette->show();
    }

}

void MainWindow::selezionaApplicazione(QString nome_app, QString path_icona, int app_position ){
    //app_position vaia da 1 a 3 e indica se l'app selezionata si trova
    //in cima, in mezzo o sotto rispetto alle altre app;

    icon_selected_app = QPixmap(path_icona);
    ui->icon_app->setPixmap(icon_selected_app.scaled(ui->icon_app->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
    ui->name_app->setText(nome_app);
    QFont font("times", 12);
    ui->name_app->setFont(font);

    if( app_position == 1 ){
       ui->up_arrow->hide();
       ui->down_arrow->show();
    }
    else if( app_position == 2 ){
        ui->up_arrow->show();
        ui->down_arrow->show();
    }
    else if( app_position == 3 ){
        ui->up_arrow->show();
        ui->down_arrow->hide();
    }

    if( !blinking_arrow_timer->isActive() && !single_app ){ // per il primo avvio: se non e' iniziato comincia il blink
        blinking_arrow_timer->start(LATENZA_BLINK);
    }

    emit operazioneCompletata();
}

void MainWindow::togliFreccie(){
    arrow_status = false;
    single_app = true;
    blinking_arrow_timer->stop();
    ui->window_freccette->hide();

}


